package Sangi;

public class TicTacToe
{
	public static void main(String[] args)
	{
		TicTacToe tic = new TicTacToe();
		int[][] table ={  {0,-1,1, 0}
		                 ,{1, 0,0, 1}
		                 ,{1,-1,0,-1}
		                 ,{1,-1,0, 0}
		               };
		
		System.out.println(tic.hasWon(table));	
	}

	public boolean hasWon(int[][] board)
	{
		int N = board.length;
		
		int row=0;
		int col=0;
		
		/* check rows*/  
		for(row=0;row<N;row++)
		{
			if (board[row][0] != -1)            //if value is -1,that cell is empty
			{
				for(col=1;col<N;col++)
				{
				if (board[row][col]	!= board[row][col-1])
				break;		
				}
				
			if (col==N)
				{
				System.out.println("check rows");
			    return true;
			    }
		   }
				
		}
		
		/*check cols */
		for(col=0;col<N;col++)
		{
			if (board[0][col] !=-1)
			{
				for(row =1;row<N;row++)
				{
					if(board[row][col] != board[row-1][col])
						break;
				}
				
				if(row==N)
				{  System.out.println("check cols");
					return true;
				}
			}
		}
		
		//check diagonal
		if(board[0][0] != -1)
		{
		
		for(row=1;row<N; row++)
		{
			if (board[row][row] != board[row-1][row-1])
					break;
		}
		
		if(row==N)
		{   
			System.out.println("diags");
		
			return true;
		}
		
		}
		
		//check rev diagonals
		if(board[N-1][0] != -1)
		{
		
		for(row=1;row<N; row++)
		{
			if (board[N-row][row-1] != board[N-row-1][row])
					break;
		}
		
		if(row==N)
		{   
			System.out.println(" rev diags");
		
			return true;
		}
		
		}
		return false;
	
   }

}