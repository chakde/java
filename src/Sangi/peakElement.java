/*A peak element is an element that is greater than its neighbors.

Given an input array where num[i] ≠ num[i+1], find a peak element and return its index.

The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.

Note:
Your solution should be in logarithmic complexity.*/

/*Binary Search- we compare middle element with its neighbors. If middle element is greater than both of its neighbors, 
 * then we return it. If the middle element is smaller than the its left neighbor, then search in left half.
 * If the middle element is smaller than the its right neighbor, then search in right half.
 */
package Sangi;
 
public class peakElement {

	public static void main(String[] args) {
		
		 int arr[] = {1, 35, 20, 4, 1, 0};
		 int n = arr.length;
	     System.out.println("Index of a peak point is "+ findPeakUtil(arr, 0, n-1, n));	  

	}
	
	static int findPeakUtil(int arr[], int low, int high, int n)
	{
	    int mid = low + (high - low)/2;  
	 
	    if ((mid==0 || arr[mid-1] < arr[mid]) && (mid == n-1|| arr[mid+1] < arr[mid]))
	        return mid;
	 
	   
	    else if
	    (mid > 0 && arr[mid-1] > arr[mid])
	        return findPeakUtil(arr, low, (mid -1), n);
	   
	    else 
	    	return findPeakUtil(arr, (mid + 1), high, n);
	}
	 


}
