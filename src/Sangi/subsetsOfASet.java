/*Given a set of distinct/duplicate integers, S, return all possible subsets.*/

package Sangi;

import java.util.ArrayList;
import java.util.Arrays;

public class subsetsOfASet {

	public static void main(String[] args) 
	{    
		int[] set = {2,2,2};
	    System.out.println(subsets(set));

	}
	
	static ArrayList<ArrayList<Integer>> subsets(int[] S)
	{  
		   // sort the given set  
		   Arrays.sort(S);  
		   // generate subsets  
		   ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();  
		   
		   results.add(new ArrayList<Integer>());  
		   //int count =0;
		   for (int i=0; i<S.length; ++i)
		   {  
		     int curSize = results.size();  
		     
		    // duplicate values
		    /* if (i > 0 && S[i] == S[i-1])
		      {   
		         count++; 
		        // System.out.println(count);
		       } else 
		       {   
		         count = 0;   
		       } 
		       */
		        
		     
		     while (curSize > 0) 
		     {   
		    	 curSize--;
		         ArrayList<Integer> res = new ArrayList<Integer>(); 
		         ArrayList<Integer> pre = results.get(curSize); //getting the previous list
		         
		        System.out.println(pre);
		        
		       /*  if (count > 0 && (pre.size() < count || pre.get(pre.size()-count) != S[i]))  
		             continue;*/
		         
		         res.addAll(pre);
		         res.add(S[i]);  
		         results.add(res);  
		       
		       
		     }  
		   }  
		   return results;  
		 }  
}
