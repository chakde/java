/*Given a list of items,return the top 3 items purchased.
 *
 * */


package Sangi;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ArrayList;

public class topNItems {

	public static void main(String[] args) 
	{
		//String[] inputStr = { "banana", "banana", "Coffee", "Coffee", "Coffee", "Coffee", "muffin", "muffin","muffin","tea"};
		String[] inputStr = { "banana", "banana", "Coffee", "Coffee","muffin", "muffin","tea"};
        int inputNum = inputStr.length;
       
        Map<String, Integer> hash = new HashMap<String, Integer>();
        for (int index = 0; index < inputNum; index++)
        {
           
            if (hash.containsKey(inputStr[index]))
            {
            	hash.put(inputStr[index], hash.get(inputStr[index]) + 1);
            }
            else
            {
            	hash.put(inputStr[index], 1);
            }
        }
        
        hash = sortByValues(hash);
        
        List MapList = new ArrayList(hash.keySet());
        for (int i = 0; i < 3; i++)
        {
            System.out.println(MapList.get(i));
        }
       

	}

	static <K extends Comparable,V extends Comparable> Map<K,V> sortByValues(Map<K,V> map)
	{
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

			@Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2)
            {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
        
        for(Map.Entry<K,V> entry: entries)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
      
        return sortedMap;
    }


}
