/* The Game of Master Mind is played as follows:
 *
 * The computer has four slots, and each slot will contain a ball that is red
 * (R), yellow (Y), green (G) or blue (B). For example, the computer might
 * have RGGB (Slot #1 is red, Slots #2 and #3 are green, Slot #4 is blue).
 *
 * You, the user, are trying to guess the solution. You might, for example,
 * guess YRGB.
 *
 * When you guess the correct color for the correct slot, you get a "hit". If
 * you guess a color that exists but is in the wrong slot, you get a
 * "pseudo-hit". Note that a slot that is a hit can never count as a 
 * pseudo-hit.
 *
 * For example, if the actual solution is RGBY and you guess GGRR, you have
 * one hit and one pseudo-hit.
 *
 * Write a method that, given a guess and a solution, returns the number of
 * hits and pseudo-hits.
 */

package Sangi;

public class masterMind {
  static int hits =0;
	static int pseudohits =0;
	
	public static void main(String[] args) {
		
		char[] sol = {'B','G','R','Y'};
		char[] guess = {'G','G','R','G'};
		
		
		
		masterMind game = new masterMind();
		int[] results = game.masterMindGame(sol, guess);
		
		System.out.println("hits " + results[0]);
		System.out.println("pseudohits " + results[1]);

	}
	
	int[] masterMindGame(char[] sol,char[] guess)
	{   
		
		
		for(int i =0;i<sol.length;i++)
		{
			if(sol[i] == guess[i])
			{
				hits++;
				sol[i] ='X';
				guess[i] = 'X';
			}
		}
		
		for(int i =0;i< sol.length;i++)
		{
			if(sol[i]!='X')
			{   
				for(int j =0;j< sol.length;j++)
				{
				if (guess[i] == sol[j])
				{
					pseudohits++;
					sol[j] ='X';
					guess[i] = 'X';
					break;
					
				}
			}
		}
	}
		
		return new int[] {hits,pseudohits};

}
}