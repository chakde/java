package Sangi;
 
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
 
class Nodell{
    int data;
    Nodell left;
    Nodell right;
    public Nodell(int data){
        this.data = data;
        this.left = null;
        this.right =null;
    }
}
class ListNodell{
    int data;
    ListNodell next;
    public ListNodell(int data){
        this.data = data;
        this.next = null;
    }
}

public class LinKedListAtDepthBT {
 
    ArrayList al = new ArrayList();
    
    public void levelOrderQueue(Nodell root)
    {
        Queue q = new LinkedList();
        int levelNodells =0;
        
        if(root==null) return;
        q.add(root);
        
        while(!q.isEmpty()){
              levelNodells = q.size();
                     
              ListNodell head = null;
                 
              ListNodell curr = null;
              
                while(levelNodells>0)
                {
                Nodell n = (Nodell)q.remove();
                ListNodell ln = new ListNodell(n.data);
               
                if(head==null)
                {  
                	System.out.println("nodehead: " + ln.data);
                    head = ln;
                    curr = ln;
                }
                else
                {   System.out.println("node: " + ln.data);
                    curr.next = ln;
                    curr = curr.next;
                }
                
                if(n.left!=null)
                	q.add(n.left);
                if(n.right!=null)
                	q.add(n.right);
                
                levelNodells--;
            }
            al.add(head);
        }
        display(al);
    }
    public void display(ArrayList al){
        Iterator it = al.iterator();
        while(it.hasNext()){
          ListNodell head = (ListNodell) it.next();
         
          while(head!=null){
              System.out.print("->" + head.data);
              head = head.next;
          }
          System.out.println("");
        }
    }
    public static void main (String[] args) throws java.lang.Exception
    {
        Nodell root = new Nodell(5);
        root.left = new Nodell(10);
        root.right = new Nodell(15);
        root.left.left = new Nodell(20);
        root.left.right = new Nodell(25);
        root.right.left = new Nodell(30);
        root.right.right = new Nodell(35);
        root.left.left.left = new Nodell(40);
        root.left.left.right = new Nodell(45);
 
        LinKedListAtDepthBT i  = new LinKedListAtDepthBT();
        
        i.levelOrderQueue(root);
    }
}
