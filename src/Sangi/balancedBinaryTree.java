package Sangi;

public class balancedBinaryTree{
	
	public static void main(String[] args)
	{
		BinarySrchTree bst = new BinarySrchTree();
		
		    bst .insert(40)
		        .insert(25);
		        //.insert(78);
		      //  .insert(10);
		       // .insert(3);
		        //.insert(17);
		        //.insert(32);
		       // .insert(30);
		       // .insert(38);
		       // .insert(78);
		      //  .insert(50);
		      //  .insert(93);	
		    
		    //int ht =bst.getHeight(bst.root);
		    int bal = bst.checkHeight(bst.root);
		    System.out.println("Tree is balanced and difference in height is " + bal);
	}   
}

class Nodeb
{
	public int value;
	public Nodeb left;
	public Nodeb right;
	
	public Nodeb(int value)
	{
		this.value = value;
	}
}

 class BinarySrchTree
{
	public Nodeb root;
	
	public BinarySrchTree insert(int value)
	{
		Nodeb node = new Nodeb(value);
		
		if (root==null)
			
		{
		 root = node;
		 return this;
		}
		
		insertRec(root,node);
		return this;
	}
	
	private void insertRec(Nodeb latestRoot,Nodeb node)
	
	{ 
	
		if(latestRoot.value > node.value)
			
		{ 
			if (latestRoot.left == null)
		{
			   latestRoot.left= node;
			   return;
		}
		else{
			
			insertRec(latestRoot.left,node);
		}
	}
		else {
				if (latestRoot.right == null)
				{
			       latestRoot.right = node;
			       return;
			     }
				else 
				{
			      insertRec(latestRoot.right, node);
			    }
			  }
		 }
	
	/* int getHeight(Nodeb root)
	 {
		 if(root==null)
			 return 0;
		 return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
	 }*/
	 
	 
	  int checkHeight(Nodeb root)
	 {
		 if (root == null)
		 {  
			 return 0;
		 }
		 
		 int leftht = checkHeight(root.left);
		 if((leftht)== -1)  
			 return -1;   //no need to traverse further
		 
		 int rightht = checkHeight(root.right);
		 if((rightht)== -1)
			 return -1;   //no need to traverse further
		 
		 int htdiff = Math.abs(leftht - rightht);
		
		 if (htdiff > 1)
			 
			 {
			 System.out.println("Tree is not balanced");
			 return -1;
			 }
		 
		 return htdiff;
		 
	 }
}