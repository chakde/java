/*In any of the country's official documents, the PAN number is listed as follows-
 
<char><char><char><char><char><digit><digit><digit><digit><char>
Your task is to figure out if the PAN number is valid or not.
A valid PAN number will have all its letters in uppercase and digits in the same order as listed above.
*/

package Sangi;
import java.util.regex.*;

public class PANNumber {

	public static void main(String[] args)
	{
		String s="ABCDE1234AY";
	    System.out.println(isValidPAN(s) ? "YES" : "NO");
    } 

    static boolean isValidPAN(String s) 
    {
	        String pattern = "([A-Z]{5})(\\d{4})([A-Z])";
	        
	        Pattern r = Pattern.compile(pattern);
	        Matcher m = r.matcher(s);
	        
	         if (m.find())
	         {
	             return true;
	         }
	         
	        return false;
	    }
	  
	}


