package Sangi;


public class Rev
{
 public static void main(String args[])
 { 
  RevList list = new RevList();
  RevList ReversedList = new RevList();
  RevList newlist = new RevList();
  //boolean test = false;
  //RevNode List = new RevNode();
     list.insert(1);
     list.insert(2);
     list.insert(5);
     list.insert(4);
   //  list.insert(3);
     //list.insert(2);
     //list.insert(9);
     list.insert(8);
     System.out.println(list);
     newlist = list;
     System.out.println(newlist);
     System.out.println(list.head);
     list.Reverse();
     ReversedList = list;
     System.out.println(ReversedList);
     System.out.println(newlist);
     System.out.println("list head"+ newlist.head);
     System.out.println("Rev head" + ReversedList.head);
     boolean test = list.compareLists(newlist.head,ReversedList.head);
     System.out.println(test);
 }
 
}

class RevNode
{
 int data;
 RevNode next;
 
 public RevNode(int data)
 {
  this.data = data;
  next = null;
 }
 
 public RevNode()
 {
  
 }
 
 public String toString()
 {
  return "Node: "+ data;
 }
 
}

class RevList
{
 
 RevNode head, tail;
 int size = 0 ;
 
 public RevList()
 { 
 }
 
 public void insert(int data)
 {
  RevNode node = new RevNode(data);
  if(size == 0)
  {
   head = node;
   tail = head;
  }
  else
  {
   tail.next = node;
   tail = node;
  }
  size ++;
 }
 
 public void Reverse()
 {   
	 
 RevNode curr = head;
 RevNode prev = null;
 RevNode next = null;
 tail = curr;
 while (curr!= null)
 {
     next = curr.next ;
     curr.next =prev;
     prev =curr;
     curr=next;
 }
 head = prev;
 //return head;
 }
 
 public boolean compareLists(RevNode head1,RevNode head2)
 {   
	// RevNode head1 = head;
   //  RevNode head2=head;
	 RevNode temp1 = head1;
	 RevNode temp2 = head2;
  
     while (temp1.next!=null & temp2.next!=null )
     {
         if (temp1.data == temp2.data)
         {  
        	// System.out.println(temp1.data);
             //System.out.println(temp2.data);
             temp1 = temp1.next;
             temp2 = temp2.next;
         }
         else return false;
     }
  
     /* Both are empty reurn 1*/
     if (temp1.next == null && temp2.next == null)
         return true;
  
     /* Will reach here when one is NULL
       and other is not */
     return false;
 }
  
 public String toString()
 {
		String retVal = "";
		if (size == 0)
			return  "( size: "+size+")";
		RevNode node = head;
		while (node != null)
		{
		  retVal = retVal + node.toString() + " ";
		   node = node.next;
		}
		return retVal + "( size: "+size+", head:"+head.data+", tail:"+tail.data+") ";
}
 

}


