/*implement stack using 2 queues*/

package Sangi;
 
import java.util.Queue;
import java.util.LinkedList;

public class stackwith2Queues {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	    Queue<Integer> q =  new LinkedList<Integer>();
	    Queue<Integer> tmp =  new LinkedList<Integer>();
	 

	    public void push(int data)
	    {    
	        if (q.size() == 0)
	            q.add(data);
	        else
	        { 
	    	    int l = q.size();
	            for (int i = 0; i < l; i++)
	                tmp.add(q.remove()); 
	            
	            q.add(data);                        
	            for (int i = 0; i < l; i++)
	                q.add(tmp.remove());
	        }
	    }    
	    
	    public int pop()
	    {
	        if (q.size() != 0)       
	        return q.remove();
	        
	        return -1;
	    }    
	   
	    public int peek()
	    {
	        if (q.size() != 0)          
	        return q.peek();
	        
	        return -1;
	    }        

}
