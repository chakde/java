/**
 * Describe an algorithm to find the smallest one million numbers in one billion
 * numbers. Assume that the computer memory CANNOT hold all one billion numbers.
 * 
 */
// O(n) space, O(lgn) time
package Sangi;

import java.util.PriorityQueue;
import java.util.Comparator;

public class modFindKSmallestEleInArray {

	public static void main(String[] args) {
		
		int[] arr = {12,1,16,3,8,4,9,5,10,18};
		int k = 4;
		int arrK[] =smallestKElements (arr,k);
		
		System.out.println(k +" smallest elements are:");
		
		for(int i = 0;i < arrK.length;i++)
		{
			System.out.println(arrK[i]);
		}
       
	}
    
	static int[] smallestKElements(int[] arr, int k)
	{  
		int[] arrK = new int[k];
		PriorityQueue<Integer> heap = new PriorityQueue<Integer>(k, new Comparator<Integer>() {

		      @Override
		      public int compare(Integer arg0, Integer arg1) {
		        return -arg0.compareTo(arg1);
		        //return arg0.compareTo(arg1); //k largest elements
		      }
		      
		    });
		
		for(int i=0;i<arr.length;i++)
		{
			heap.add(arr[i]);
			
			if(heap.size() >k)
			{
				heap.poll();
				
				
			}
		}
		
		int idx =0;
		while(!heap.isEmpty())
		{
			arrK[idx++] = heap.poll();
		}
		
		return arrK;
    
	}
}
