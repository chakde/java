package Sangi;

import java.util.Stack;

public class evaluateRevPolishNotation {

	public static void main(String[] args)
	{
		String[] exp = { "2", "1", "20","+","*" };
		System.out.println(evaluateExpression(exp));
	}

	static int evaluateExpression(String[] exp)
	{
		int result =0;
		Stack<String> s = new Stack<String>();
		
		String operators ="+-*/";
		
		for (String t : exp) 
		{
			if (!operators.contains(t)) 
			{ 
				s.push(t);
			} 
			
			else
				
			{
				int a = Integer.valueOf(s.pop());
				int b = Integer.valueOf(s.pop());
				
				switch(t)
				{
				case "+":
					s.push(String.valueOf(a+b));
					break;
					
				case "-":
					s.push(String.valueOf(a-b));
					break;
					
				case "*":
					s.push(String.valueOf(a*b));
					break;
					
				case "/":
					s.push(String.valueOf(a/b));
					break;
				}
	      }
    }
		result = Integer.valueOf(s.pop());
		
		return result;
		 
}
}