/*Problem Statement

There are N integers in an array A. All but one integer occur in pairs.
Your task is to find the number that occurs only once.*/

package Sangi;

import java.util.Arrays;

public class LonelyInteger {
	
	
	public static void main(String[] args)
	{
		int[] numbers = {1,2,1,2,4,3,5,4,5};
		Arrays.sort(numbers);
		int x = lonely(numbers);
		System.out.println(x);
	}

	static int lonely(int[] numb)
	{ 
		int i;
		for(i=0;i<numb.length;i=i+2)
		{
			if (numb[i]!=numb[i+1])
			return numb[i];
		}
		
		return 0;
	}
}
