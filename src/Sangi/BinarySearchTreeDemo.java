package Sangi;

public	 class BinarySearchTreeDemo{
	
	
	
	public static void main(String[] args)
	{
		    BinarySearchTree bst = new BinarySearchTree();
		    bst .insert(40)
		        .insert(103)
		        .insert(78)
		        .insert(10)
		        .insert(3)
		        .insert(17)
		        .insert(32)
		        .insert(30)
		        .insert(38)
		        .insert(78)
		        .insert(50)
		        .insert(93);
		    
		    
		    System.out.println("Inorder traversal");
		    bst.printInorder();
		  /*  boolean isbst =bst.isBst(bst.root);
		    System.out.println(isbst);*/
		 
		    System.out.println("Preorder Traversal");
		    bst.printPreorder();
		 
		    System.out.println("Postorder Traversal");
		    bst.printPostorder();
		 
		    System.out.println("The minimum value in the BST: " + bst.findMinimum());
		    System.out.println("The maximum value in the BST: " + bst.findMaximum());

	}   
}

class Node10
{
	public int value;
	public Node10 left;
	public Node10 right;
	
	public Node10(int value)
	{
		this.value = value;
	}
}

 class BinarySearchTree
{   
	 public static int last_printed = Integer.MIN_VALUE;
	public Node10 root;
	
	public BinarySearchTree insert(int value)
	{
		Node10 node = new Node10(value);
		
		if (root==null)
			
		{
		 root = node;
		 return this;
		}
		
		insertRec(root,node);
		return this;
	}
	
	private void insertRec(Node10 latestRoot,Node10 node)
	
	{ 
	
		if(latestRoot.value > node.value)
			
		{ 
			if (latestRoot.left == null)
		{
			   latestRoot.left= node;
			   return;
		}
		else{
			
			insertRec(latestRoot.left,node);
		}
	}
		else {
				if (latestRoot.right == null)
				{
			       latestRoot.right = node;
			       return;
			     }
				else 
				{
			      insertRec(latestRoot.right, node);
			    }
			  }
		 }
	
	public int findMinimum(){
	   if (root == null) {
		        return 0;
		      }
		       Node10 currNode = root;
		       while (currNode.left != null) {
		         currNode = currNode.left;
		       }
		       return currNode.value;
		   }
		    
		   public int findMaximum() {
		      if (root == null) {
		        return 0;
		      }
		   
		   	    Node10 currNode = root;
		   	    while (currNode.right != null) {
		   	      currNode = currNode.right;
		   	    }
		   	    return currNode.value;
		   	  }
		   	 
		   	  /**
		   90	   * Printing the contents of the tree in an inorder way.
		   91	   
		   	 * @return */
		   	  public void printInorder()
		   	  {
		   	    printInOrderRec(root);
		        System.out.println("");
		   	  }
		   	 
		   	  /**
		   	   * Helper method to recursively print the contents in an inorder way
		   	   */
		   	  private void printInOrderRec(Node10 currRoot) {
		   	    if (currRoot == null) {
		   	      return;
		   	    }
		   	
		   	     printInOrderRec(currRoot.left);
		   	     System.out.print(currRoot.value + ", ");
		   	     printInOrderRec(currRoot.right);
		   	
		   	  }
		   	    /*boolean isBst(Node10 currRoot) {
			   	    if (currRoot == null)
			   	    {
			   	      return true;
			   	    }
			   	
			   	     if(!isBst(currRoot.left))
			   			 return false;
			   	     if(currRoot.value <=last_printed)
			   	    	 return false;
			   	     last_printed = currRoot.value;
			   	     
			   	     //System.out.print(currRoot.value + ", ");
			   	     if (!isBst(currRoot.right))
			   	     return false;
			   	     
			   	     return true;
			   	
			   	  }
		   	 */
		   	  /**
		   110	   * Printing the contents of the tree in a Preorder way.
		   111	   */
		   	  public void printPreorder() {
		   	    printPreOrderRec(root);
		   	    System.out.println("");
		   	  }
		   	 
		   	  /**
		   118	   * Helper method to recursively print the contents in a Preorder way
		   	   */
		   	  private void printPreOrderRec(Node10 currRoot) {
		   	    if (currRoot == null) {
		   	      return;
		   	    }
		   	    System.out.print(currRoot.value + ", ");
		       printPreOrderRec(currRoot.left);
		      printPreOrderRec(currRoot.right);
		    }
		   
	public void printPostorder()
	{
		   printPostOrderRec(root);
		   System.out.println("");
    }
		   
	private void printPostOrderRec(Node10 currRoot)
	{
		     if (currRoot == null)
		     {
		      return;
		     }
		    printPostOrderRec(currRoot.left);
		    printPostOrderRec(currRoot.right);
		    System.out.print(currRoot.value + ", ");
		  	 
   }
}