package Sangi;



public class firstAncestorOFTwoNodes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	  Noda root = new Noda(5);
		
      root.left = new Noda(10);
      root.right = new Noda(15);
     
      root.left.left = new Noda(20);
      root.left.right = new Noda(25);
    
      root.right.left = new Noda(30);
      root.right.right = new Noda(35);
     
      root.left.left.left = new Noda(40);
      root.left.left.right = new Noda(45);
      

     
      Noda res;
      res = findcommonAnc(root,root.left.left.left,root.left.left.right);
      System.out.println(res.data);
      
	}

	static Noda findcommonAnc(Noda root,Noda a,Noda b) //specify the root of the two nodes for which u r seraching the common ancestor
	{
		Noda left =null;
		Noda right = null;
		
		if(root==null)
		{
			return null;
		}
		if(root==a||root ==b) //if root = a or b,then your root is common ancestor,root parent is root so a root's immediate ancestor is itself
		{
			return root;
		}
		
		left = findcommonAnc(root.left,a,b); //keep on traversing towards left to check if the element is present in the left subtree..if not u will get a null
		right =findcommonAnc(root.right,a,b);
		
		if(left!=null && right!=null)
		{
			return root;  //different subtrees
		}
		
		if(left==null && right==null)
		{
			System.out.println("Elements not present in tree");
		}
		
		return (left != null) ? left:right;
	}
	
}



class Noda{
  int data;
  Noda left;
  Noda right;
 
  
  public Noda(int data)
  {
      this.data = data;
      this.left = null;
      this.right =null;
      
  }
}




