Implement a  Queue class using 2 stacks
Approach-

Queue is fifo,stack is lifo.
So,if we join 2 stacks,such that it pushes new elements  into 1 stack and pops from other(stack2),it is similar to queue.
For this,we will pop elements from stack1 onto stack 2
so,stack 2 is having all the elements,so we can peek and pop/remove from this stack.
Whenever we need to insert /push new elements,push that in stack 1.