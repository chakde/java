/*You are given a list of N numbers a1,a2,…,an. For each element at position i (1≤i≤N), we define Left(i) and Right(i) as: 
Left(i) = closest index j such that j < i and a[j]>a[i]. If no such j exists then Left(i) = 0. 
Right(i) = closest index k such that k > i and a[k]>a[i]. If no such k exists then Right(i) = 0.

We define IndexProduct(i) = Left(i) * Right(i). You need to find out the maximum IndexProduct(i) among all i.*/

package Sangi;

public class maxIndexProd {

	static int[] a ={5,4,3,4,5};
	static int n =a.length;
	
	public static void main(String[] args) {
		
		int max_pro = 0;
		int pro =0;
		
		int[] l =  new int[n];
		int[] r =  new int[n];
		
		for(int i=1;i<n-1;i++)
		 {	
			 left(i,l);
			 right(i,r);
			 
		    if( l[i] == 0 || r[i] == 0)
		        pro = 0;
		    else
		        pro = l[i] * r[i];
		    
		    if( pro > max_pro)
		        max_pro = pro;
		 }
		System.out.println("Maximum index product is: "+ max_pro);
	}
	
	static void left(int i,int[] l)
	{ 
	
	    for(int x=i-1;x>=0;x--)
	    { 
	        if (a[x] > a[i])
	        	
	            {
	        	l[i] = x+1;
	            return;
	            }
	    }
	    l[i] =  0;
	    
	}
	
	static void right(int i,int[] r)
	{ 
	    for(int x=i+1;x<n;x++)
	    { 
	        if (a[x] > a[i])
	        { 
	        	r[i] = x+1;
	            return;
	        }
	        
	    r[i] =  0;
	    }
	}
	 
	

}
