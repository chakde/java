/*Given an input string, reverse the string word by word.
 * 
 * Clarification:
What constitutes a word?
A sequence of non-space characters constitutes a word.

Could the input string contain leading or trailing spaces?
Yes. However, your reversed string should not contain leading or trailing spaces.

How about multiple spaces between two words?
Reduce them to a single space in the reversed string.
*/
 
package Sangi;

public class revWordsString {
	
	static String s ="  Oh     my      god  ";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(ReverseWords());

	}

	static String ReverseWords()
	{
		String[] arr = s.split(" ");		
		StringBuilder sb = new StringBuilder();
		
		for(int i=arr.length-1;i>=0;i--)
		{   	
			if(arr[i].trim()!=null && !arr[i].trim().isEmpty())
			{   			
			    sb.append(arr[i]).append(" ");
				
			}		
		}		
		return sb.length() == 0 ? "" : sb.substring(0, sb.length() - 1).trim();
	}
}
