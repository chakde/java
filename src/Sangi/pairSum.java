/*Find all pairs of integers within an array which sum to a specified value*/

package Sangi;


public class pairSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] numbers = { 2, 4, 3, 5, 7, 8, 9 };

		
		printPairs(numbers,7);
	}
	
	 static void printPairs(int[] array, int givenSum) 
	{ 
		int sum =0;
		
		for (int i = 0; i < array.length; i++)
		{
			for (int j = i + 1; j < array.length; j++) 
			{ 
				sum = array[i]+array[j]; 
				
	            if (sum == givenSum)
	                 { 
	            	
	            	System.out.println("Pair with given sum is:" + "(" + array[i]+","+array[j]+")");
	            	
	                } 
	            }
			}
		}

}
