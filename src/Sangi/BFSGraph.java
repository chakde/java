package Sangi;

import java.util.Queue;
import java.util.LinkedList;


public class BFSGraph {
    
	
	public static void main(String[] args) {
//                  0  1  2  3  4  5  6  7  8
//===================================================
int[][] conn = {  { 0, 1, 0, 1},// 0, 0, 0, 0, 1 },  // 0
                  { 1, 0, 0, 0},// 0, 0, 0, 1, 0 },  // 1
                  { 0, 0, 0, 1},// 0, 1, 0, 1, 0 },  // 2
                  { 1, 0, 1, 0}};// 1, 0, 0, 0, 0 },  // 3
                 // { 0, 0, 0, 1, 0, 0, 0, 0, 1 },  // 4
//{ 0, 0, 1, 0, 0, 0, 1, 0, 0 },  // 5
//{ 0, 0, 0, 0, 0, 1, 0, 0, 0 },  // 6
//{ 0, 1, 1, 0, 0, 0, 0, 0, 0 },  // 7
//{ 1, 0, 0, 0, 1, 0, 0, 0, 0 } };// 8


             Graph G = new Graph(conn);
             G.bfs();
             

	}
}
	
	class Graph{
		
		int[][] adjMatrix;
		int noOfNodes;
		int rootNode =0;
		
		boolean[] nodeIsVisited;
	 
	 Graph(int[][] mat) //constructor 1
	 {
		 int i,j;
		 
		 noOfNodes = mat.length;
		 adjMatrix = new int[noOfNodes][noOfNodes];
		 nodeIsVisited = new boolean[noOfNodes];
		 
		 for (i=0;i<noOfNodes;i++)
			 for(j=0;j<noOfNodes;j++)
				 adjMatrix[i][j] = mat[i][j];
		 
	 }
	 
	 public void bfs()
	 {
		 Queue<Integer> q = new LinkedList<Integer>();
		 
		 q.add(rootNode);
		 nodeIsVisited[rootNode] = true;
		 
		 System.out.println("BFS order is: ");
		 
		 while(!q.isEmpty())
		 {   
			 int node = q.peek();
			 
			 int child = getUnvisitedChildNode(node);
			 getPath(node);
			 
			 if(child!=-1)
			 {
			 q.add(child);
			 nodeIsVisited[child]=true;
			 
			 printNode(child); //prints the bfs order
			 
			 }
			 
			 else
			 {
				 q.remove();
			 }
		 }
		 
	 }
		
	 int getUnvisitedChildNode(int n)
		{
			int j;
			for(j =0;j<noOfNodes;j++)
			{
				if ( adjMatrix[n][j] > 0 )
		         {  	
					System.out.println("path exists between node " + n +" and node "+ j);
					
				if(!nodeIsVisited[j])
				{
					return j;
				}
				
		         }
			}
			return -1;
		}
	 
	 void getPath(int n)
	 { 
		 int i,j;
		 for (i=0;i<noOfNodes;i++)
			 for(j=0;j<noOfNodes;j++)
				 if (adjMatrix[i][j] == 1)
				 {
					 //System.out.println("path exists between node " + i +" and node "+ j);
					 //return 0;
				 }
		 
			//return -1;
	 }
	 void printNode(int n)
	   {
	      System.out.println(n);
	   }
	}


	
