package Sangi;

import java.util.Arrays;


public class Lexico {

	public static void main(String[] args) {
		
		String input = "sangeeda";

        boolean IsValid = false;
        
        char temp;
        char[] inputArr;
        char[] strRem2Arr;
        
        for (int index = input.length() - 1; index >= 0; index--)
        {
            char lastChar = input.charAt(index);
            for (int ptr = index - 1; ptr >= 0; ptr--)
            {
                if (lastChar > input.charAt(ptr))
                {
                    IsValid = true;
                    
                    inputArr = input.toCharArray();
                    
                    //swap
                    temp = inputArr[ptr];
                    inputArr[ptr] = inputArr[index];
                    inputArr[index] = temp;
                    
                    //get the remaining substring
                    String updInput = new String(inputArr);
                    String strRem1 = updInput.substring(0, ptr + 1);
                    String strRem2 = updInput.substring(ptr + 1);
                    
                    //sort remaining string
                    strRem2Arr  = strRem2.toCharArray();
                    Arrays.sort(strRem2Arr);
                    String rem = new String(strRem2Arr);
                    
                    //get the final lexico string
                    rem = strRem1 + rem;
                    System.out.println("Next lexico higher string is: " + rem);


                    break;
                }

            }

            if (IsValid)
                break;

        }
    }
}
