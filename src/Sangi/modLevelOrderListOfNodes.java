/*Given a binary tree, return the level order traversal of its nodes' values.
 *  (ie, from left to right, level by level).
 * */
 
package Sangi;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class modLevelOrderListOfNodes {

	public static void main(String[] args) {
		
		NodeLev root = new NodeLev(2);
		root.left = new NodeLev(1);
		root.right = new NodeLev(4);
		System.out.println("Level wise nodes are: " + levelOrder(root));

	}
	
	static ArrayList<ArrayList<Integer>> levelOrder(NodeLev root)
	{
		ArrayList<ArrayList<Integer>> finalList = new ArrayList<ArrayList<Integer>>();
		
		Queue<NodeLev> q = new LinkedList<NodeLev>();
		
		if (root==null)
			   return finalList;
		
		q.add(root);
		q.add(null); //to demark the level
		
		while(!q.isEmpty())
		{
			NodeLev curr = q.remove();
			
			ArrayList<Integer> list = new ArrayList<Integer>();
			
			while(curr!=null)  //processing one level
			{
				list.add(curr.data); //while processing adding data to list
				
				if(curr.left!=null)
					q.add(curr.left);
				if(curr.right!=null)
					q.add(curr.right);
				
				curr = q.remove();
			}
			
			finalList.add(list); //add processed level
			
			if(!q.isEmpty())
				q.add(null);
		}
		
		return finalList;
	}

}

class NodeLev
{
	NodeLev left;
	NodeLev right;
	int data;
	
	public NodeLev(int value)
	{
		left = null;
		right =null;
		this.data = value;
	}
}