package Sangi;
public class Duplicate
{
	public static void main(String[] args)
	{
		//int inputArr[] = {1,2,3,3,4,5,5}; Different style of providing input as array
		int inputArr[] = new int[7];
		inputArr[0]= 1;
		inputArr[1]= 1;
		inputArr[2]= 3;
		inputArr[3]= 3;
		inputArr[4]= 4;
		inputArr[5]= 5;
		inputArr[6]= 5;      
		int size = inputArr.length;
		System.out.println("Size of array is " +size );
		printRepeating(inputArr,size);
	}
	
	   public static void printRepeating(int[] testArr,int size) 
	   {  
	      int i,j;
	      for (i=0;i<size;i++)
	    	  for (j=i+1;j<size;j++)
	    		  if (testArr[i]==testArr[j])
	    			 
	    		   System.out.println("Duplicate element is " + testArr[j] );  //can use testArr[i] as well
	   }
	    
}