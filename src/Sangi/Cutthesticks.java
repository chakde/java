/*You are given N sticks, where the length of each stick is a positive integer. A cut operation is
 *  performed on the sticks such that all of them are reduced by the length of the smallest stick.
 *  Given the length of N sticks, print the number of sticks that are cut in subsequent cut operations.
 */
package Sangi;

import java.util.Arrays;
import java.util.Collections;

public class Cutthesticks {

	public static void main(String[] args) 
	{
		  Integer[] array = new Integer[] {
		            new Integer(1),
		            new Integer(2),
		            new Integer(3),
		            new Integer(4),
		            new Integer(3),
		            new Integer(3),
		            new Integer(2),
		            new Integer(1)
		            
		     };
		        
		  cuts(array);
		
		
	}
	
	static void cuts(Integer[] array)
	{ 
		Arrays.sort(array, Collections.reverseOrder());
		
		int count =0;
		int count1 = 0;
		int count2 =0;
		
		for(int i =0;i<array.length;i++)
		{  
			count1=0;
			
			for(int j=0;j<array.length - count;j++)
			{				
					array[j]=array[j] - array[array.length -count -1];
					count1++;
			}
			if(count1!=0)
				System.out.println(count1);
			
		    for(int j=0;j<array.length - count;j++)
			{				
					if(array[j]==0)
					count2++;
			}
			count = count2;
		}
	}

}
