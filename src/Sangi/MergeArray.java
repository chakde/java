/*Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.*/
package Sangi;


public class MergeArray
{
public  static void main(String[] args)
{  
	int[] A = {13,33,55,88,9000,0,0,0,0,0,0,0,0};
    int[] B = {11,21,68,99,201};
    
    int lastA = 5;
    int lastB = 5;
    
    int index = (lastB + lastA);
    
	merge(A,B,lastA,lastB);
	
	System.out.println("Final sorted array is:");
	for(int j =0; j <index; j++)
	{
	System.out.println(A[j]);
	}
}

static void merge(int[] a,int[] b,int lastA,int lastB)
{
	int indexA = lastA-1; //4
	int indexB = lastB-1; //4
	
	int indexMerged = (lastB + lastA)-1; //9
	
	while (indexA >=0 && indexB >=0)
	{
		if (a[indexA] > b[indexB])
		{
			a[indexMerged] = a[indexA];
			indexMerged--;
			indexA--;
		}
		
		else
		{
			a[indexMerged] = b[indexB];
			indexMerged--;
			indexB--;
		}
	}
	
	while (indexB>=0)
		{
		a[indexMerged]=b[indexB];
	    indexMerged--;
	    indexB--;
	    }
}
}