package empowr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

class PuzzleSolver
{
    public static String[] Dictionary = {"OX","CAT","TOY","AT","DOG","CATAPULT","T"};

    static boolean IsWord(String testWord)
    {
        if( Arrays.asList(Dictionary).contains(testWord))
            return true;
        return false;
    }

    public int FindWords(char[][] puzzle)
    {
        if(puzzle == null || puzzle.length == 0)
            return 0;

        int numOfWords = 0;

        numOfWords += checkWords(scanHorizontally(puzzle));
        numOfWords += checkWords(scanVertically(puzzle));
        numOfWords += checkWords(scanDiagonally(puzzle));

        return numOfWords;
    }

    //method to scan rows of 2D char array
    private String[] scanHorizontally(char[][] puzzle)
    {
        String[] words = new String[puzzle.length];

        for(int i=0;i<puzzle.length;i++)
        {
            words[i] = String.valueOf(puzzle[i]);
        }

        return words;
    }

    //method to scan columns of 2D char array
    private String[] scanVertically(char[][] puzzle)
    {
        return scanHorizontally(transposeMatrix(puzzle));
    }

    //method to extract diagonals of 2D char array
    private String[] scanDiagonally(char[][] puzzle)
    {
        int numOfRows = puzzle.length;
        int numOfCols = puzzle[0].length;

        ArrayList<String> listOfWords = new ArrayList<String>();

        for(int row=numOfRows-1; row>=0; row--) // Traversing through rows
        {
            int i = row;
            int j = 0;
            String word = "";

            while(i < numOfRows && j < numOfCols) // Bounding condition for diagonals
            {
                word += puzzle[i++][j++];
            }

            listOfWords.add(word);
        }

        for(int col=1; col < numOfCols; col++) // Traversing through columns
        {
            int i = 0;
            int j = col;
            String word = "";

            while(i < numOfRows && j < numOfCols) // Bounding condition for diagonals
            {
                word += puzzle[i++][j++];
            }

            listOfWords.add(word);
        }

        return listOfWords.toArray(new String[listOfWords.size()]);
    }

    // method to identify dictionary of words from a given set of strings
    private int checkWords(String[] words)
    {
        HashSet<String> list = new HashSet<String>(10);

        for(int i=0;i<words.length;i++)
        {
            checkAllSubStringsRec(words[i], list);
            checkAllSubStringsRec(reverseWordRec(words[i]), list);
        }

        return list.size();
    }

    //checkString method to check the word against dictionary and count the number of occurrences
    private void checkAllSubStringsRec(String word, HashSet<String> list)
    {
        for(int c = word.length(); c > 0; c-- )
        {
            String subword = word.substring(0,c);

            if(IsWord(subword))
                list.add(subword);
        }

        if(word.length() != 1)
            checkAllSubStringsRec(word.substring(1), list);
    }

    //helper method to reverse a string
    private String reverseWordRec(String s)
    {
        if ((null == s) || (s.length() <= 1))
            return s;

        return reverseWordRec(s.substring(1)) + s.charAt(0);
    }

    //helper method to find the transpose of 2D char array
    private char[][] transposeMatrix(char[][] puzzle)
    {
        int numOfRows = puzzle.length;
        int numOfCols = puzzle[0].length;

        char[][] transposedPuzzle = new char[numOfCols][numOfRows];

        for(int i=0;i<numOfRows;i++)
        {
            for(int j=0;j<numOfCols;j++)
            {
                transposedPuzzle[j][i] = puzzle[i][j];
            }
        }

        return transposedPuzzle;
    }
}

public class PuzzleSolverTest
{
    static PuzzleSolver puzzleSolver = new PuzzleSolver();

    public static void main(String[] args)
    {
        PuzzleSolverTest puzzleSolverTest = new PuzzleSolverTest();
        puzzleSolverTest.testAll();
    }

    public void testAll() {
        testNull();
        testEmpty();
        testEmpty_3X3();
        test3x3();
        test3x4();
        test1x1();
        test3x8();
        test1x3();
        test3x1();
        test2x2_similar();
        test3x1_similar();
        test1x3_similar();
        test3x3_orderchange();
    }

    private void printResults(char[][] charArr, int result, int expected, String title)
    {
        System.out.println(title);
        System.out.print("Input Array: ");
        System.out.println(Arrays.deepToString(charArr));
        System.out.println("Output \t: " + result);

        if(result == expected)
            System.out.println("PASS - " + title);
        else
            System.out.println("*** FAIL *** - " + title);
    }

    public void testNull()
    {
        char[][] charArr = null;

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                0,
                "Test for NULL matrix");
    }

    public void testEmpty()
    {
        char[][] charArr = new char[0][0];

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                0,
                "Test for empty matrix - 0x0");
    }

    public void testEmpty_3X3()
    {
        char[][] charArr = new char[3][3];

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                0,
                "Test for empty matrix - 3x3");
    }

    public void test3x3()
    {
        char[][] charArr =
                {{'C','A','T'},
                {'X','Z','T'},
                {'Y','O','T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                8,
                "Test 3X3 matrix");
    }

    public void test3x3_orderchange()
    {
        char[][] charArr =
                {{'C','T','A'},
                {'X','Z','T'},
                {'Y','O','T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                7,
                "Test 3X3 matrix order change");
    }

    public void test1x1()
    {
        char[][] charArr = {{'T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                3,
                "Test 1X1 matrix");
    }

    public void test2x2_similar()
    {
        char[][] charArr =
                {{'T','T'},
                {'T','T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                3,
                "Test 2X2 matrix with similar chars");
    }

    public void test1x3_similar()
    {
        char[][] charArr = {{'T','T','T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                3,
                "Test 1x3 matrix with similar chars");
    }

    public void test3x1_similar()
    {
        char[][] charArr =
                {{'T'},
                {'T'},
                {'T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                3,
                "Test 3x1 matrix with similar chars");
    }

    public void test3x8()
    {
        char[][] charArr =
                {{'C','A','T','A','P','U','L','T'},
                {'X','Z','T','T','O','Y','O','O'},
                {'Y','O','T','O','X','T','X','X'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                22,
                "Test 3X8 matrix");
    }

    public void test3x4()
    {
        char[][] charArr =
                {{'D','T','G','T'},
                {'X','O','O','O'},
                {'Y','Y','T','Y'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                6,
                "Test 3x4 matrix");
    }

    public void test1x3()
    {
        char[][] charArr = {{'C','A','T'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                5,
                "Test 1x3 matrix with different chars");
    }

    public void test3x1()
    {
        char[][] charArr =
                {{'T'},
                {'O'},
                {'Y'}};

        printResults(
                charArr,
                puzzleSolver.FindWords(charArr),
                4,
                "Test 3x1 matrix with different chars");
    }
}
