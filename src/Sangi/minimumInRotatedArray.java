/*
Suppose a sorted array is rotated at some pivot unknown.
(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element if -

1) the array has no duplicates.
2) The array may contain duplicates.
*/

package Sangi;

public class minimumInRotatedArray {

	public static void main(String[] args) 
	{
		int arr[] = {12,12,20,10,11,11};
		System.out.println("min is: " + findMin(arr,0,5));
	}
	
	static int findMin(int arr[], int low, int high)
	{
	    //no rotation
	    if (high < low)
	    	return arr[0];
	 
	    // only one element 
	    if (high == low)
	    	return arr[low];
	 
	    // Find mid
	    int mid = low + (high - low)/2; 
	 
	    if (mid < high && arr[mid+1] < arr[mid])
	       return arr[mid+1];
	    
	    /*case 2 - if duplicate entries occur*/
	    
	   /* if (arr[low] == arr[mid] && arr[high] == arr[mid])
	        return Math.min(findMin(arr, low, mid-1), findMin(arr, mid+1, high));
	   */
	
	    if (mid > low && arr[mid] < arr[mid - 1])
	       return arr[mid];
	 
	    if (arr[high] > arr[mid])
	       return findMin(arr, low, mid-1);
	    
	    return findMin(arr, mid+1, high);
	}
	 

}
