package Sangi;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Hashtable;

public class RobotMoves {
	
	 

	public static void main(String[] args) {
		
		ArrayList<Point> list1 = new ArrayList<Point>();
		Hashtable<Point,Boolean> h = new  Hashtable<Point,Boolean>();
		int[][] arr ={ {0,1,1,1},
				       {0,1,1,1},
				       {0,1,1,1},
				       {0,0,0,0}
		             };
				
		
		RobotMoves robo = new RobotMoves();
		
		robo.getpath(3,3,list1,h,arr);
		System.out.println("Here is my path: " + list1);
		

	}
	
	boolean isFree(int x,int y,int[][] travelArr)
	{
	if (travelArr[x][y] == 0)
	{
	 
	  return true;
	}
	else
      {
		
        return false;
      }
	}
	
	boolean getpath(int x,int y,ArrayList<Point> list, Hashtable<Point,Boolean> h,int[][] arr)
	{
		

	Point p = new Point(x,y);

	if (h.containsKey(p))
	{
	return h.get(p);
	}
    
	if(x==0 && y==0)
	{
		return true;
	}
	
	boolean success = false;

	if (x>=1 && isFree(x-1,y,arr))
	{// move left
	success = getpath(x-1,y,list,h,arr);
	}

	if (!success && y>=1 && isFree(x, y-1, arr))
	{//move up
	success = getpath(x,y-1,list,h,arr);
	}

	if(success)
	{
	list.add(p);
	
	}
	else
	{
	System.out.println("I am stuck!!");
	}
	
	
	h.put(p, success);
	
	return success;
	}



}
