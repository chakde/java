//Given N integers, count the number of pairs of integers whose difference is K.
package Sangi;

import java.util.Arrays;

public class pairDiff {

	public static void main(String[] args) {
		
		 int arr[] =  {1, 5, 3, 4, 2};
		 int n = arr.length;
		 int diff = 2;
		 
		// int i = countPairsWithDiffK(arr, n, diff);
		// System.out.println("Method1-Count of pairs with given diff is: "+i);
		 
		 int j =countPairs(arr,2);
		 System.out.println("Method2 -Count of pairs with given diff is: "+j);
		   
	}



  
static int countPairsWithDiffK(int arr[], int n, int k)
{
    int count = 0;
    Arrays.sort(arr);  // Sort array elements
 
    int l = 0;
    int r = 0;
    
    while(r < n)
    {
         if(arr[r] - arr[l] == k)
        {
              count++;
              l++;
              r++;
        }
         else if(arr[r] - arr[l] > k)
              l++;
         else 
              r++;
    }   
    return count;
}
 
static int countPairs(int[] array, int givendiff) 
	{ 
		int diff =0;
		int count =0;
		
		for (int i = 0; i < array.length; i++)
		{
			for (int j = i + 1; j < array.length; j++) 
			{ 
				diff = Math.abs(array[i]-array[j]); 
				
	            if (diff == givendiff)
	                 { 
	            	
	            	count++;
	            	
	                } 
	            }
			}
		return count;
		}


}