package Sangi; 

import java.util.Stack; 

public class minStack
{ 
public Stack<Integer> basic; 
public Stack<Integer> hold_min; 

public minStack()
{ 
this.basic=new Stack<Integer>(); 
this.hold_min=new Stack<Integer>(); 
} 

public void push(int newItem)
{ 
  if(newItem<=this.getmin())
   { 
    this.hold_min.push(newItem); 
    } 
this.basic.push(newItem);	
} 

public int pop()
{ 
   if(this.basic.isEmpty())
   { 
     return Integer.MIN_VALUE; 
   } 
   
  int value=this.basic.pop(); 

if(value==this.getmin())
  { 
   this.hold_min.pop(); 
  } 
return value; 
} 

public int getmin()
{ 
if(this.hold_min.isEmpty())
 { 
  return Integer.MAX_VALUE; 
 } 
else
{ 
return this.hold_min.peek(); 
} 

} 


public static void main(String[] args) 
{ 
minStack test=new minStack(); 
test.push(6); 
test.push(223); 
test.push(7); 
test.push(16); 
test.push(13); 
test.push(70); 

System.out.println("Minimum element is "+ test.getmin()); 
System.out.println("Popped element is "+ test.pop()); 
} 

}

