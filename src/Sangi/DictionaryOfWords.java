
package Empowr;
import java.util.Arrays;

public class PuzzleSolver
{
    public static String[] Dictionary = {"OX","CAT","TOY","AT","DOG","CATAPULT","T"};

    static boolean IsWord(String testWord)
    {
    	if( Arrays.asList(Dictionary).contains(testWord))
            return true;
        return false;
    }	
	
	public static int FindWords(char[][] puzzle)
	{
	 	return -1;
	}
}

public class PuzzleSolverTest
{
    public static void main(String[] args)
    {
        PuzzleSolverTest puzzleSolverTest = new PuzzleSolverTest();
        puzzleSolverTest.testAll();
    }

    public void testAll() {
        testNull();
        testEmpty();
        testEmpty_3X3();
        test3x3();
        test3x4();
        test1x1();
        test3x8();
        test2x2_similar();
        test3x1_similar();
        test1x3_similar();
    }

    private void printResults(char[][] charArr, int result, int expected, String title)
    {
        System.out.println(title);
        System.out.print("Input Array: ");
        System.out.println(Arrays.deepToString(charArr));
        System.out.println("Output \t: " + result);

        if(result == expected)
            System.out.println("PASS - " + title);
        else
            System.out.println("*** FAIL *** - " + title);
    }

    public void testNull()
    {
        char[][] charArr = null;
        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                0,
                "Test for NULL matrix");
    }

    public void testEmpty()
    {
        char[][] charArr = new char[0][0];
        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                0,
                "Test for empty matrix - 0x0");
    }

    public void testEmpty_3X3()
    {
        char[][] charArr = new char[3][3];
        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                0,
                "Test for empty matrix - 3x3");
    }

    public void test3x3()
    {
        char[][] charArr =
                {{'C','A','T'},
                {'X','Z','T'},
                {'Y','O','T'}};

        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                8,
                "Test 3X3 matrix");
    }

    public void test1x1()
    {
        char[][] charArr = {{'T'}};
        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                1,
                "Test 1X1 matrix");
    }

    public void test2x2_similar()
    {
        char[][] charArr =
                {{'T','T'},
                {'T','T'}};

        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                4,
                "Test 2X2 matrix with similar chars");
    }

    public void test1x3_similar()
    {
        char[][] charArr = {{'T','T','T'}};
        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                3,
                "Test 1x3 matrix with similar chars");
    }

    public void test3x1_similar()
    {
        char[][] charArr =
                {{'T'},
                {'T'},
                {'T'}};
        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                3,
                "Test 3x1 matrix with similar chars");
    }

    public void test3x8()
    {
        char[][] charArr =
                {{'C','A','T','A','P','U','L','T'},
                {'X','Z','T','T','O','Y','O','O'},
                {'Y','O','T','O','X','T','X','X'}};

        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                22,
                "Test 3X8 matrix");
    }

    public void test3x4()
    {
        char[][] charArr =
                {{'D','T','G','T'},
                {'X','O','O','O'},
                {'Y','Y','T','Y'}};

        printResults(
                charArr,
                PuzzleSolver.FindWords(charArr),
                6,
                "Test 3x4 matrix");
    }
}

