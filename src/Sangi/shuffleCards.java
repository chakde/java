package Sangi;

public class shuffleCards {
	
	public static void main(String[] args)
	{   
		shuffleCards shuff = new shuffleCards();
		int[] cards ={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
				      21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,
				      38,39,40,41,42,43,44,45,46,47,48,49,50,51,52 };
		
		int[] shuffle = shuff.shuffleArrayRec(cards,52);
		
		for (int j=0;j<shuffle.length;j++)
		{
			System.out.println(shuffle[j]);
		}
	}
	
	 int rand (int low,int high)
	{   
		return low + (int)(Math.random()* (high -low+1));
	}
	 
	 int shiftIndex(int i)
	 {
		 int k1 = rand(0,i);
		 int shift;
		 //System.out.println("k1: "+k1);
		 shift = Math.abs(i-k1);
		 //System.out.println(shift);
		 return shift;
	 }
	 
	 int prev(int index,int[] cards)
	 {
		 if(index==0)
			 return cards.length-1;
		 else
			 return index-1;
	 }

	 int[] shuffleArrayRec(int[] cards,int i)
	{  
		int numOfSwaps =0;
		int backTrack = cards.length-1;
		 
		for(int j =0;j<cards.length;j++) //increment front variable
		 {  
			int shiftInd =shiftIndex(j);
			int k = rand(shiftInd,backTrack);
			
			int temp = cards[k]; //3 way swap
			cards[k] = cards[j];
			cards[j] = cards[backTrack];
			cards[backTrack]= temp;
			
			numOfSwaps++;
			
			backTrack--; //decrement back variable
		
		if (j == backTrack)
			break;
	
		
	}   
		System.out.println("Number of Swaps:" + numOfSwaps);
		return cards;
	}
}
