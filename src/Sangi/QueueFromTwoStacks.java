package Sangi;

import java.util.*;

public class QueueFromTwoStacks {
	
	Stack<Integer> newStack,oldStack;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		

	}
	
	public QueueFromTwoStacks()
	{
		newStack =new Stack<Integer>();
		oldStack = new Stack<Integer>();
	}
	
	public int size()
	{
		return newStack.size() + oldStack.size();
	}
	
	public void add(int value)
	{
		newStack.push(value);
	}
	
	public int remove()
	{   
		populateOld();
		return oldStack.pop();
	}
	
	public int peek()
	{   
		populateOld();
		return oldStack.peek();
	}
	
	public void populateOld() //main step to push elements from new to old
	{   
		if(oldStack.isEmpty())
		{ 
			while(!newStack.isEmpty())
			{
		      oldStack.push(newStack.pop());
			}
		}
	}


}
