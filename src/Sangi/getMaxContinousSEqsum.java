package Sangi;

public class getMaxContinousSEqsum {

	public static void main(String[] args) {
		
		getMaxContinousSEqsum cont = new getMaxContinousSEqsum();
		
		int[] a = {5,-9,6,-2,3};
		//int[] a = {-3,-9,-2};
		
		int res = cont.getMaxSum(a);
		System.out.println("Max sum is: "+ res);
	}
	
	int getMaxSum(int[] a)
	{   
		int maxsum =a[0];
		int curr_max=a[0];
		
		for(int i =1;i<a.length;i++)
		{
			
			curr_max+=a[i];
			
			curr_max = Math.max(a[i],curr_max);
			maxsum = Math.max(maxsum, curr_max);
			
		}
		return maxsum;
	}

}
