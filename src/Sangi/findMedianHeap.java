//package Sangi;
//
//import java.util.Scanner;
//import java.util.Arrays;
//import java.util.Stack;
//
//public class findMedianHeap {
//
//	public static void main(String[] args) {
//		
//		Scanner sc = new Scanner(System.in);
//        System.out.println("Enter size of Binary heap");
//        int n = sc.nextInt();
//       // BinaryHeap bh = new BinaryHeap(n);
//        int[] heap = new int[n];
//        System.out.println("Enter elements of Binary heap");
//        
//		 for(int ctr=0;ctr<n;ctr++)
//		 {		  
//			     int n1 = sc.nextInt();
//			   //  System.out.println(n1);
//			     heap[ctr] = n1;
//			     median(n1);
//			    // bh.insert(n1);	
//		         //bh.printHeap();
//		 }
//		 
//		 sc.close();
//		 
//		 for(int ctr=0;ctr<heap.length;ctr++)
//		 {		  
//			    // int n1 = sc.nextInt();
//			     System.out.println(heap[ctr]);
//		 }
//        
//       
//        
//	}
//
//}
//
//class BinaryHeap
//{ 
//	private int[] heap;
//	
//	public BinaryHeap(int capacity)
//	{
//		heap = new int[capacity];
//	}
//	
//	/* void insert(int x)
//	{   
//		for(int i=0;i<heap.length;i++)
//		{
//      
//		heap[i] = x;
//		System.out.println("heap:" +heap[i] + "i pos:" + i);
//		
//		}
//		
//	}*/
//		
//	void printHeap()
//	{   
//		int j=0;
//		while(j<heap.length)
//		{
//      
//		System.out.println("print" + heap[j]);
//		j++;
//		}
//		
//	}
//	
//		
//        
//    }
//	
//	
/*package Sangi;

import java.util.Queue;
import java.util.LinkedList;

public class findMedianHeap{

public static void main(String[] args)
{
	Queue<Integer> input = new LinkedList<Integer>();
	input.add(1);
	String flag = "odd";
	//getMedian(flag,input);
	
	
	input.add(3);
	int var2 =3;
	//flag ="even";
	//getMedian(flag,input);
	input.add(6);
	input.add(7);
	input.add(9);
	input.add(11);
	flag ="even";
	getMedian(flag,input);

}

 static void getMedian(String flag,Queue<Integer> input)
{
	int median =0;
	//int count=0;
	int var1 = 0; int var2=0;
	//Queue<Integer> input = new LinkedList<Integer>();
	//count++;
	
if(flag=="odd")
{
	median = input.peek();
	var1=input.peek();
}



if(flag=="even")
	
{   
	var1 =input.remove();
	var2= input.peek();
	median = (var1 + var2)/2;
}
System.out.println(median);
}

}*/

package Sangi;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
 
// Given a stream of unsorted integers, find the median element in sorted order at any given time.
// http://www.ardendertat.com/2011/11/03/programming-interview-questions-13-median-of-integer-stream/
public class findMedianHeap{
	
	public static void main(String[] args) {
		
		findMedianHeap heap = new findMedianHeap();
		
		heap.add(1);
		System.out.println(heap.getMedian()); // should be 1
		
		heap.add(5);
		heap.add(10);
		heap.add(12);
		heap.add(2);
		System.out.println(heap.getMedian()); // should be 5
		
		heap.add(3);
		heap.add(8);
		heap.add(9);
		System.out.println(heap.getMedian()); // should be 6.5
	}
 
	public Queue<Integer> minHeap;
	public Queue<Integer> maxHeap;
	public int numOfElements;
	
	public findMedianHeap() 
	{
		minHeap = new PriorityQueue<Integer>();
		maxHeap = new PriorityQueue<Integer>(10, new MaxHeapComparator()); 
		numOfElements = 0;
	}
	
	public void add(Integer num)
	{
		maxHeap.add(num);
		if (numOfElements%2 == 0) 
		{
			if (minHeap.isEmpty())
			{
				numOfElements++;
				return;
			}
			else if (maxHeap.peek() > minHeap.peek())
			{
				Integer maxHeapRoot = maxHeap.poll();
				Integer minHeapRoot = minHeap.poll();
				maxHeap.add(minHeapRoot);
				minHeap.add(maxHeapRoot);
			} 
		} 
		
		else
		{
			minHeap.add(maxHeap.poll());
		}
		numOfElements++;
	}
	
	public Double getMedian() 
	{
		if (numOfElements%2 != 0)
			return new Double(maxHeap.peek());
		else
			return (maxHeap.peek() + minHeap.peek()) / 2.0; 
	}
	
	private class MaxHeapComparator implements Comparator<Integer> {
		@Override
		public int compare(Integer o1, Integer o2) {
			return o2 - o1;
		}
	}
	
	
}