/*Given an array of integers and an integer k, find out whether there there are two distinct indices i and j 
 * in the array such that nums[i] = nums[j] and the difference between i and j is at most k.
 */

package Sangi;

import java.util.HashMap;

public class duplicatesWithDiff {

	static int[] nums = {1,2,3,4,2,5,2};
	
	public static void main(String[] args)
	{
		
		System.out.println(DupwithDiff(1));

	}
	
	static boolean DupwithDiff(int k)
	{
        if (k < 0) 
        	return false;
        
        HashMap<Integer,Integer> h  = new HashMap<Integer,Integer>();
        
        for (int i=0;i<nums.length;i++)
        {
            if (h.containsKey(nums[i]) && i - h.get(nums[i]) <= k)
            {
               
                    return true;
            }
            
            h.put(nums[i],i) ;
            
      
	}
        return false;

	}
}
