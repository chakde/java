package Sangi;

public class printPathsSumToValue {
	
	public static void main (String[] args) throws java.lang.Exception
    {
        Nodp root = new Nodp(1);
        root.left = new Nodp(2);
        root.right = new Nodp(3);
        root.left.left = new Nodp(4);
        root.left.right = new Nodp(2);
        root.left.left.left = new Nodp(2);
        root.left.left.right = new Nodp(3);
        root.left.right.left = new Nodp(4);        
        
        int sum =8;
        int[] pathDisplay = new int[20];
        findSum(root,sum,pathDisplay,0);
 
    }
	
	
	public static void findSum(Nodp node,int sum,int[] path,int level)
    {
      if(node == null)
    	  return;
      
	path[level] = node.data;
	
	int t=0;
	
	for(int i=level;i>=0;i--)
	{
		t =t+path[i];
		
		
		if(t==sum)
		{
			print(path,i,level);
		}
	}
			
			findSum(node.left,sum,path,level+1);
			findSum(node.right,sum,path,level+1);
			
	}
	
	

	
	static void print(int[] path,int strt,int end)
	{   
		System.out.println("Path is -");
		
		for(int i=strt;i<=end;i++)
		{
			System.out.print(path[i]+"\n");
		}
		
	}
}

class Nodp{
    int data;
    Nodp left;
    Nodp right;
    public Nodp(int data){
        this.data = data;
        this.left = null;
        this.right =null;
    }
}
