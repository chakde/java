package Sangi;

public class ShortestSubSeq{
	
	public static void main(String[] args)
	{
		int[] array = {1,2,4,7,10,11,7,12,6,7,16,18,19};
		findUnsortedSeq(array);
	}
	
static int findEndOfLeftSubSeq(int[] array)
	{ 
    for (int i =1;i<array.length;i++)
	{
		if(array[i] <array[i-1])
			return i-1;
	}
		return array.length-1;
	}


static int findStartOfRightSubSeq(int[] array)
{ 
for (int i =array.length-2;i>=0;i--)
{
	if(array[i] >array[i+1])
		return i+1;
}
	return 0;
}

static int shrinkLeft(int array[],int min_index,int start)
{
	int comp= array[min_index];
	for (int i=start-1;i>=0;i--)
	{
		if(array[i]<=comp)
			return i+1;
	}

return 0;
}

static int shrinkRight(int array[],int max_index,int start)
{
	int comp= array[max_index];
	for (int i=start;i<array.length;i++)
	{
		if(array[i]>=comp)
			return i-1;
	}

return array.length-1;
}

static void findUnsortedSeq(int[] array)
{
	int end_left = findEndOfLeftSubSeq(array);
	int start_right =findStartOfRightSubSeq(array);
	int min_index = end_left+1;
	int max_index = start_right-1;
	
	if(min_index>=array.length)
		return; //already sorted
	
	for(int i = end_left;i<=start_right;i++)
	{
	  if(array[i] <array[min_index])
		  min_index=i;
	  if(array[i] >array[min_index])
		  max_index=i;
	  
	}

   int m= shrinkLeft(array,min_index,end_left);
   int n = shrinkRight(array,max_index,start_right);
   
   System.out .println(m + " " + n);

}
}