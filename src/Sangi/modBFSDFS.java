package Sangi;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class modBFSDFS {
	
	static int[][] adjMatrix = {  { 0, 1, 0, 1},
                                  { 1, 0, 0, 0},
                                  { 0, 0, 0, 1},
                                  { 1, 0, 1, 0}};
	
	private static int NNodes = adjMatrix.length;
	static boolean[] visited = new boolean[NNodes];

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	static void BFS(Node root)
	{
		Queue<Integer> q = new LinkedList<Integer>();
		
		
		q.add(root.data);
		
		visited[root.data] = true;
		
		while(!q.isEmpty())
		{   
			int n = q.remove();
			
			int child = getUnvisitedChild(n);
			
			if(child !=-1)
			{
				q.add(child);
				visited[child] = true;
				printNode(child);
			}
			
			else
			{
			
			q.remove();
			}
			
		}
		clearVisited();
		
		
	}
	
	static void DFS(Node root)
	{
		Stack<Integer> s = new Stack<Integer>();
		
		s.push(root.data);
		visited[root.data] = true;
		
		while(!s.isEmpty())
		{
			int n = (int) s.peek();
			
			int child = getUnvisitedChild(n);
			
			if(child!=-1)
			{
				s.push(child);
				visited[child] = true;
				printNode(child);
			}
			else
			{
			s.pop();
			}
			
		}
		
		clearVisited();
		
	}
	
	static int getUnvisitedChild(int n)
	{  
		int j;

	   for (j = 0; j < NNodes; j++ )
	    {
		 if (adjMatrix[n][j] > 0 )
	        {
		      if(!visited[n])
		      {
			    return j;
		      }
	        }
	    }
	   return -1;
   }
	
	
	static void clearVisited()
	{
		for (int i = 0; i < visited.length; i++ )
	    {
			visited[i] =false;
		}
	}
	
	static void  printNode(int n)
	{
		System.out.println(n);
	}
}