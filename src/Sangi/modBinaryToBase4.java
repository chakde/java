/*given a string in base 2,convert it to base 4*/

package Sangi;

public class modBinaryToBase4 {

	public static void main(String[] args) {
		String s="1111101";
		System.out.println(bintoBaseFour(s));

	}
	
	static String bintoBaseFour(String s)
	{
		if (s.length()%2!=0)
			s ='0'+s;
		
		StringBuilder newStr = new StringBuilder();
		
		for(int i =0;i< s.length();i=i+2)
		{
			if(s.charAt(i)=='0' && s.charAt(i+1)=='0')
				newStr.append('0');
			if(s.charAt(i)=='0' && s.charAt(i+1)=='1')
				newStr.append('1');
			if(s.charAt(i)=='1' && s.charAt(i+1)=='0')
				newStr.append('2');
			if(s.charAt(i)=='1' && s.charAt(i+1)=='1')
				newStr.append('3');
		}
		
		return newStr.toString();
		
	}

}
