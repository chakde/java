//in oder traversal

package Sangi;

public class nextNodeBST {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Nodl root = new Nodl(5);
		root.parent =root;
        root.left = new Nodl(10);
        root.right = new Nodl(15);
        root.left.parent =root;
        root.right.parent =root;
        root.left.left = new Nodl(20);
        root.left.right = new Nodl(25);
        root.left.left.parent = root.left;
        root.left.right.parent = root.left;
        root.right.left = new Nodl(30);
        root.right.right = new Nodl(35);
        root.right.left.parent = root.right;
        root.right.right.parent = root.right;
        root.left.left.left = new Nodl(40);
        root.left.left.right = new Nodl(45);
        root.left.left.left.parent = root.left.left;
        root.left.left.right.parent = root.left.left;
 
       
        Nodl res;
        res = findNextNodeInBST( root.right.right);
        System.out.println(res.data);
        
	}

	static Nodl findNextNodeInBST(Nodl n)
	{
		if(n == null)
			return null;
		
		if(n.right!=null)
		{
			return leftMostChild(n.right);
		}
		
		else
		{ 
        
			Nodl q = n;
			Nodl x = n.parent;
			
			
			
			while(x!=null && x.left!=q)
			{
				q=x;
				x=x.parent;
				
				if(q==x)
				{
				System.out.println("No next node");
				System.out.println("Root node is:");
				break;
				}
						 
				
			}
			return x;
		}
		
	}
	
	
	static Nodl leftMostChild(Nodl n)
	{
		if (n== null)
			return n;
		
		while(n.left!=null)
		{
			n=n.left;
		}
		return n;
	}
}


 
class Nodl{
    int data;
    Nodl left;
    Nodl right;
    Nodl parent;
    
    public Nodl(int data)
    {
        this.data = data;
        this.left = null;
        this.right =null;
        this.parent=null;
    }
}




