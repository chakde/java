/*The professor is conducting a course on Discrete Mathematics to a class of N students.
 *  He is angry at the lack of their discipline, and he decides to cancel the class if there 
 *  are less than K students present after the class starts.
 * Given the arrival time of each student, your task is to find out if the class gets cancelled or not.
 * If the arrival time of a given student is a non-positive integer (ai<=0), then the student enters before the 
 * class starts.
 *  If the arrival time of a given student is a positive integer (ai>0),
 *  the student enters after the class has started.If a student enters the class exactly when it starts (ai=0), 
 *  the student is considered to have entered before the class has started.
*/
package Sangi;
public class AngryProfessor {
	
	
	public static void main(String[] args)
	{
		
		int k =3;
		int count =0;
		
		int[] time ={-3,-1,2,1};
		
		for (int i=0;i<time.length;i++)
			{
			if( time[i] <= 0)
			count++;
		    }
		
		if(count<k)
			System.out.println("YES");
		else
			System.out.println("NO");
	}
	

}
