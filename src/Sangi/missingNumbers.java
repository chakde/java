package Sangi;

import java.util.ArrayList;
import java.util.Collections;

public class missingNumbers {

	public static void main(String[] args)
	{  
		int[] list1 = {203, 204 ,205, 206, 207 ,208 ,203 ,204 ,205 ,206};
	    int[] list2 = {203 ,204 ,204, 205, 206 ,207, 205 ,208 ,203, 206 ,205, 206 ,204};
		permutation(list1,list2); 
	}
	
	static void permutation(int[] s,int[] t)
	{   
		int[] letters = new int[256];
		ArrayList<Integer> arr = new ArrayList<Integer>() ;
		
		for(int i=0;i<s.length;i++)
		{
			int k = s[i];
			letters[k]++;	
		}
		
		for(int i=0;i<t.length;i++)
		{
			int k = t[i];
			if(--letters[k]<0)
			{   
				arr.add(k);
			}
			
		}
		
		Collections.sort(arr);
		System.out.println("Missing numbers:");
		
		 for(Integer counter: arr){
				System.out.println(counter);
			}
 
	}

}
