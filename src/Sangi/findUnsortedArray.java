/*Given an unsorted array arr[0..n-1] of size n, find the minimum length subarray
 *  arr[s..e] such that sorting this subarray makes the whole array sorted.
 *  return the values of s and e.
 */

package Sangi;

public class findUnsortedArray {

	public static void main(String[] args) {
		
		//int[] arr ={1,2,4,7,10,11,7,12,6,7,16,18,19};
		int[] arr ={1,2,4,7,10,11,1,2,66,5,11,8};//,19};
		findUnsortedArray findInd = new findUnsortedArray();
		findInd.findUnsortedArrayIndices(arr);
	}
	
	void findUnsortedArrayIndices(int[] arr)
	{   
		int len = arr.length;
		int s= 0;
		int e =len-1;
		
		//for loop for finding end of left increasing subsequence
		for(s= 0; s<len-1; s++)
		{
			if (arr[s] > arr[s+1])
				break;
		}
		
		if (s == len-1)
		  {
		    System.out.println("The complete array is sorted");
		    return;
		  }
		
		//for loop for finding start of right incresing subsequence
		for(e = len -1; e>=0; e--)
		{
			if(arr[e-1] > arr[e])
				break;
		}
		
		int max = arr[s];
		int min = arr[s];
		
		//find the min and max of the middle section
		for(int i =s+1;i<=e;i++)
		{
			if(arr[i] > max)
				max = arr[i];
			if(arr[i] < min)
				min = arr[i];
		}
		
		//finding the start of unsorted subarray
		for(int i =0;i<s;i++)
		{
			if(arr[i] > min)
			{
				s=i;
				break;
			}
		}
		
		//finding the end of unsorted subarray
		for(int i = len-1;i>=e;i--)
		{
			if(arr[i] < max)
			{
				e=i;
				break;
			}
		}
		
		System.out.println("starting index is: " + s + " and ending index is: "+ e);
	}
	
	

}
