/*Given a string s and a dictionary of words dict, determine if s can be segmented into a
 *  space-separated sequence of one or more dictionary words.

	For example, given
	s = "leetcode",
	dict = ["leet", "code"].

	Return true because "leetcode" can be segmented as "leet code"package Sangi;
*/

//check for each substring if it is present in dict or not as below

package Sangi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class wordBreak {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String elements[] = {"leet", "code"};
	    Set set = new HashSet(Arrays.asList(elements));
			
		System.out.println(wordcheck("leetcode",set));

	}
	
	static boolean wordcheck(String s, Set<String> dict)
	{

		int len = s.length();
	    boolean flag = false;
	               
	    for(int i = 1; i <= len; i++)
	    {
	        String subStr = s.substring(0, i);
	        
	        if(dict.contains(subStr))
	        {
	                  if(subStr.length() == s.length())
	                  {
	                     return true;
	                  } 
	                  
	          flag = wordcheck(s.substring(i), dict);
	        }
	    }
	     
	    return flag;
   }

}
