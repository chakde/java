//given a 2D matrix,find the number of islands(a group of connected 1s)

package Sangi;

public class connectedcellsDFS {

	 public static void main(String[] args) {
	        
		 int count = 0;
	       
		 int arr[][] = {    {1, 1, 0, 1, 0},
	                        {0, 1, 0, 0, 1},
	                        {1, 0, 0, 1, 1},
	                        {0, 1, 0, 0, 0},
	                        {1, 0, 1, 0, 1}};
		 
	        boolean visited [][] = new boolean[5][5];
	       
	        for(int  i =0; i<5;i++)
	        {
	            for(int j = 0; j<5;j++)
	            {
	                if(arr[i][j] == 1 && !visited[i][j])
	                {
	                    count++;
	                    connectedCells(arr,visited,i,j);
	                }

	            }
	        }

	        System.out.println("Number of connected cells is " + count);
	   
	    }

   
   static void connectedCells(int arr[][],
                              boolean  visited[][],
                              int i, int j)
    {
        if(i < 0 || j < 0 )
            return;
        
        if(i >= arr.length)
            return;
        
        if(j >= arr[0].length)
            return;
        
        if(visited[i][j])
            return;
        
        if(arr[i][j] == 1)
        {
            visited[i][j] = true;
            
            //call for all neighbours
            
            connectedCells(arr, visited,i-1, j-1);
            connectedCells(arr, visited,i-1, j);
            connectedCells(arr, visited,i-1, j+1);
            connectedCells(arr, visited,i, j-1);
            connectedCells(arr, visited,i, j+1);
            connectedCells(arr, visited,i+1, j-1);
            connectedCells(arr, visited,i+1, j);
            connectedCells(arr, visited,i+1, j+1);
        }

    }

   }