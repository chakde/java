package Sangi;

public class isSubtreeOfATree {
	
	public static void main (String[] args) throws java.lang.Exception
    {
        Nodest root = new Nodest(5);
        root.left = new Nodest(10);
        root.right = new Nodest(15);
        root.left.left = new Nodest(20);
        root.left.right = new Nodest(25);
        root.right.left = new Nodest(30);
        root.right.right = new Nodest(35);
        root.left.left.left = new Nodest(40);
        root.left.left.right = new Nodest(45);
        
        Nodest root1 = new Nodest(10);
        root1.left = new Nodest(20);
        root1.right = new Nodest(25);
        
      if(ifsubtreeValue(root,root1))
       System.out.println("subtree");
 
    }
	
	
	public static boolean ifsubtreeValue(Nodest t1, Nodest t2)
    {
       if (t2 == null)
            return true;
       
        if (t1 == null)
            return false;
        
        if (t1.data == t2.data)
        {
        	if (ifsubtreeValue(t1.left, t2.left) && ifsubtreeValue(t1.right, t2.right)) //go on comparing rest of the nodes if one 
        		                                                                         //of nodes in both tress matches
                return true;
        }
        
        return ifsubtreeValue(t1.left, t2) || ifsubtreeValue(t1.right, t2); //if values does not match ,then keep traversing left 
                                                                            //and right subtrees of t1 and compare with t2
    }
	
	
	
}


class Nodest{
    int data;
    Nodest left;
    Nodest right;
    public Nodest(int data){
        this.data = data;
        this.left = null;
        this.right =null;
    }
}
