package Sangi;

import java.util.Arrays;

public class PuzzleSolverr
{
    public static String[] Dictionary = {"OX","CAT","TOY","AT","DOG","CATAPULT","T"};

    static boolean IsWord(String testWord)
    {
    	if( Arrays.asList(Dictionary).contains(testWord))
            return true;
        return false;
    }	
    
    public static void main(String[] args)
	{   
    	 char[][] puzzle = {{'C','A','T'},
		            {'X','Z','T'},
		            {'Y','O','T'}};
    	 PuzzleSolverr ps = new PuzzleSolverr();
    	 
    	ps.FindWords(puzzle);
	}
	
	public  int FindWords(char[][] puzzle)
	{  
		extractRows(puzzle);
		extractColumns(puzzle);
	 	return -1;
	}
	
	 void extractRows(char[][] puzzle)
	{   
		int row = puzzle.length;
		String[] strOfRows = new String[row];
		
		for(int i=0;i<row;i++)
		{
			char[] str = puzzle[i];
			String rows = new String(str);
	 
			strOfRows[i] =rows;
		}
		
	}
	 
	 void extractColumns(char[][] puzzle)
	{   
		int col = puzzle[0].length;
		
		char[][] colTranspose = transpose(puzzle);
		extractRows(colTranspose);
		
	}
	 
	char[][] transpose(char[][] puzzle)
	{   
		int row =puzzle.length;
		int col =puzzle[0].length;
		
		char[][] transPuzzle = new char[row][col];
		
		for(int i=0;i< row;i++)
		{
			for(int j=0;j<col;j++)
			{
				transPuzzle[j][i] =puzzle[i][j];
			}
		}
		
      return transPuzzle;
    
	}
	
	
}


	

	
	
	




