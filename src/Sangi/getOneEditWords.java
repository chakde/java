package Sangi;

//import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Queue;
import java.util.LinkedList;

public class getOneEditWords {

	public static void main(String[] args)
	{   
		Set<String> dic = new HashSet<String>();
		
	    dic.add("hot");
	    dic.add("dot");
	    dic.add("dog");
	    dic.add("lot");
	    dic.add("log");
	    dic.add("cog");
	    
		getOneEditWords getwords = new getOneEditWords();
		
		LinkedList<String> path = new LinkedList<String>();
		path = getwords.transform("hit","cog",dic);
		System.out.println("Path is: " + path);

	}
	
	LinkedList<String> transform(String startWord,String stopWord,Set<String> dictionary)
	{
		Queue<String> queue = new LinkedList<String>();
		Set<String> visitedSet = new HashSet<String>();
		Map<String,String> backtrackMap = new TreeMap<String,String>();
		
		queue.add(startWord);
		visitedSet.add(startWord);
		
		while(!queue.isEmpty())
		{
			String w = queue.remove();
			
			for(String v:getOne(w))
			{
				if(v.equals(stopWord))
				{  
					
					LinkedList<String> list = new LinkedList<String>();
					list.add(v);
					
					while(w!=null)
					{
						list.add(0,w);
						w = backtrackMap.get(w);
					}
					
					return list;
				}
			
			
			if(dictionary.contains(v))
			{  
				//System.out.println("found word " + v);
				if(visitedSet.contains(v)==false)
				{
				queue.add(v);
				visitedSet.add(v);
				backtrackMap.put(v,w);
				}
			}
		}
		}
		
		return null;
	}
	
	
	
	 Set<String> getOne(String word)
	{
		Set<String> words = new TreeSet<String>();
		
		for (int i=0;i<word.length();i++)
		{
			char[] wordArray = word.toCharArray();
			
			for(char c ='a';c<='z';c++)
			{
				if(c!=word.charAt(i))
				{
					wordArray[i]=c;
					words.add(new String(wordArray));
				}
			}
		}
	return words;
	}

}
