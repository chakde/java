package Sangi;

public  class checkBST{

	public static void main(String[] args)
	{
		  checkBST objBST = new checkBST();
		  Node root =  objBST.new Node(4);		  
		  root.left  = objBST.new Node(2) ;
		  root.right       = objBST.new Node(5);
		  root.left.left  = objBST.new Node(10);
		  root.left.right = objBST.new Node(3); 
		 
		
		  if(checkBST1(root,Integer.MIN_VALUE,Integer.MAX_VALUE))
			{
			  
		      System.out.println("bst");
		     }
		  else
			  System.out.println("Not a BST");
		
	}
	

public class Node
{
	public int value;
	public Node left;
	public Node right;
	
public Node(int value)
{
	this.value = value;
	
}

}


 static boolean checkBST1(Node n,int min,int max)
 {
	if(n== null)
		return true;
	
	if (n.value<=min || n.value>max)
	   return false;
	
	if(!checkBST1(n.left,min,n.value) ||
			!checkBST1(n.right,n.value,max))
	  return false;
	
	return true;
	
	
	
	
	
	
}
}