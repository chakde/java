package Sangi;


class Nodedel{
	int data;
	Nodedel next;
	public Nodedel(int data){
		this.data = data;
		next = null;
	}
	
	public String toString(){
		return "Node: "+ data;
	}
}

class LinklistDel{
	Nodedel head, tail;
	int size = 0 ;
	public LinklistDel(){	
	}
	
	public void insert(int data){
		Nodedel node = new Nodedel(data);
		if(size == 0){
			head = node;
			tail = head;
		}else{
			tail.next = node;
			tail = node;
		}
		size ++;
	}
	
	public void delete(int index){
		if(index > size || index < 0)
			return;
		int cnt = 1;
		Nodedel node = head;
		while(cnt < index - 1){
         	cnt ++;
         	node = node.next;
		}
		if(size == 1){
			head = tail = null;
		}else{
			if(index == 1){
				head = head.next; 
			}
			if(index == size){
				node.next = node.next.next;
				tail = node;	
			}
			else{
				node.next = node.next.next;
			}
		}
		System.out.println("Removing "+index+"th Node");
		size--;
	}
	
	
	public String toString(){
		String retVal = "";
		if (size == 0)
			return  "( size: "+size+")";
		Nodedel node = head;
		while (node != null){
		  retVal = retVal + node.toString() + " ";
		   node = node.next;
		}
		return retVal + "( size: "+size+", head:"+head.data+", tail:"+tail.data+") ";
	}
}

public class DelnodeMid {
    public static void main(String[] args) {
    	LinklistDel list = new LinklistDel();
    	System.out.println(list);
    	list.insert(1);
    	list.insert(2);
    	System.out.println(list);
    	list.delete(2);
    	list.delete(1);
    	list.insert(3);
    	list.insert(4);
    	list.insert(5);
    	System.out.println(list);
    	//list.insert(2);
    	//list.insert(2);
    	//list.find(2);
    	System.out.println(list);
   }
}
