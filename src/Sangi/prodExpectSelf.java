/*Given an array of n integers where n > 1, nums, return an array output such that output[i] is 
  equal to the product of all the elements of nums except nums[i].
  Solve it without division and in O(n).
  For example, given [1,2,3,4], return [24,12,8,6].
  Follow up:
  Could you solve it with constant space complexity? 
  (Note: The output array does not count as extra space for the purpose of space complexity analysis.)

*/

package Sangi;

public class prodExpectSelf {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int[] arr ={2,2,3,4,5};
		prodExcept(arr);
	}
	
	static void prodExcept(int[] arr)
	{
		int prodLeft =1;
		int prodRight=1;
		
		int len =arr.length;
		int[] out = new int[len];
		
		for(int i=0;i<len;i++)
		{
			out[i] =1;
		}
		
		for (int i =0;i<len-1;i++)
		{   
			prodLeft*=arr[i];
			prodRight *= arr[len-1-i];
			
			out[i+1]*=prodLeft;
			out[len-i-2]*=prodRight;
		}
		
		System.out.println("Output array is:");
		for(int i =0;i<len;i++)
		{
			System.out.println(out[i]);
		}
		
	}

}
