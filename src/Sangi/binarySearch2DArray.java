/*Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

Integers in each row are sorted from left to right.
The first integer of each row is greater than the last integer of the previous row.
For example,

Consider the following matrix:

[
  [1,   3,  5,  7],
  [10, 11, 16, 20],
  [23, 30, 34, 50]
]
Given target = 3, return true.
*/

package Sangi;

public class binarySearch2DArray {

	public static void main(String[] args) {
		

		int[][] matrix = { {1,   3,  5,  7},
		                   {10, 11, 16, 20},
		                   {23, 30, 34, 50}};
		
		System.out.println(search(matrix,3));

	}
	
	static boolean search(int[][] matrix,int x)
	{  
 
        int m = matrix.length;
        int n = matrix[0].length;
 
        int start = 0;
        int end = m*n-1;
 
        while(start<=end)
        {
            int mid=(start+end)/2;
            
            int midX=mid/n;
            int midY=mid%n;
            
            System.out.println("x: "+ midX);
            
            System.out.println("y: "+ midY);
 
            if(matrix[midX][midY]==x) 
                return true;
 
            if(matrix[midX][midY]<x)
            {
                start=mid+1;
            }
            else
            {
                end=mid-1;
            }
        }
 
        return false;
    }
}
