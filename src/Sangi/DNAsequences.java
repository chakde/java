/*All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T, for example: "ACGAATTCCG".
It is sometimes useful to identify repeated sequences within the DNA.
Write a function to find all the 10-letter-long sequences (substrings) that occur more than once in a DNA molecule.
*/

package Sangi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DNAsequences {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list1 = findRepeatedSeq("AAAAACCCCCAAAAACCCCCAAACC");
        System.out.println(list1);
	}
	
	static List<String> findRepeatedSeq(String s)
	{
		HashMap<String,Integer> h= new HashMap<String,Integer>();
		List<String> list= new ArrayList<String>();
		int len =s.length();
		
		for(int i =0;i<=len-10;i++)
		{
			String key = s.substring(i,i+10);
			
		
			if(h.containsKey(key))
			{
				h.put(key, h.get(key)+1);
				
				if(h.get(key)>1)
					list.add(key);
			}
			else
			{
				h.put(key,1);
			}
		}
		
		return list;
	}

}
