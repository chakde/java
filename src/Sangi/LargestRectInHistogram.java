/*Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, 
 * find the area of largest rectangle in the histogram.
 */

package Sangi;

import java.util.Stack;

public class LargestRectInHistogram
{
	 public static void main(String[] args)
	 { 		 
		
		Stack<Integer> stack =new Stack<Integer>();
		Stack<Integer> stack1 =new Stack<Integer>();
		
		stack.push(7);
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(5); 
		
		int maxArea = 0;
		
		for(int index=0;index<stack.size();index++)
		{
			int area = 	calcMAx(stack,stack1);
			
			if(maxArea<area)
				maxArea = area;			
		}
		System.out.println("Area of largest rectangle is: " + maxArea );
	}
 
	static int calcMAx(Stack<Integer> stack,Stack<Integer> stack1)  
	 {		   
		   int min = 1000000;
		   int prevMax = 0;
		   int max = 0;
		   int size=0;
		   
		 while (!stack.isEmpty())
		 {
			 int height = stack.pop(); //number popped from stack,height of building  or length of rectangle
			 stack1.push(height); 
			 
			 size=stack1.size(); //size of stack1 gives the breadth of rectangle
			
			 if(height < min)
				 min=height;
			 
			 max = maxArea(min, size);
			 
			 if(prevMax < max)
			 {
			 	prevMax = max;
			 }
		 }		 
		 return prevMax;
	 }
	
	static int maxArea(int ht,int size)
	{ 
		int area =0;
	    area = (ht*size);
		return area;
	}

  
}
