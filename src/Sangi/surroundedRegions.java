/*Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.
*/
package Sangi;

public class surroundedRegions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		char arr[][] = {    {'X', 'X', 'X', 'X'},
				            {'X', 'O', 'O', 'X'},
				            {'X', 'O', 'O', 'X'},
				            {'X', 'O', 'X', 'X'}};
				
				
		solve(arr);  


	}

	
	static void solve(char[][] board) 
	{
	    if(board == null || board.length==0) 
	        return;
	 
	    int numOfRows = board.length;
	    int numOfCols = board[0].length;
	 
	    //change O's on 1st & last column
	    for(int i=0;i<numOfRows;i++)
	    {
	        if(board[i][0] == 'O')
	        {
	            change(board, i, 0);
	        }
	 
	        if(board[i][numOfCols-1] == 'O')
	        {
	            change(board, i,numOfCols-1);
	        }
	    }
	 
	    //change O's on top & bottom row
	    for(int j=0; j<numOfCols; j++)
	    {
	         if(board[0][j] == 'O')
	         {
	            change(board, 0,j);
	         }
	 
	        if(board[numOfRows-1][j] == 'O')
	        {
	            change(board, numOfRows-1,j);
	        }
	    }
	 
	   
	    for(int i=0;i<numOfRows;i++)
	    {  
	        for(int j=0; j<numOfCols; j++)
	        {  
	            if(board[i][j] == 'O')
	            {
	                board[i][j] = 'X';
	            }
	            else if(board[i][j] == '#')
	            {
	                board[i][j] = 'O';
	            }
	            
	          
	        }
	    }
	}
	 
	static void change(char[][] board, int i, int j)
	{
	    if(i<0 || i>=board.length || j<0 || j>=board[0].length) 
	        return;
	 
	    if(board[i][j] != 'O')
	        return;
	 
	    board[i][j] = '#';
	     
	    //4 surroundings
	    change(board, i-1, j);
	    change(board, i+1, j);
	    change(board, i, j-1);
	    change(board, i, j+1);
	}
}
