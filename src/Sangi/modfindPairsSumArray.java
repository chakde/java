/*How to find all Pairs in Array of Integers whose Sum is equal to a given Number*/

package Sangi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class modfindPairsSumArray {

	public static void main(String[] args) {
		
		int[] arr ={0, 14, 0, 4, 4, 8, 3, 5, 8};
		
		//findPairsUsingHash(arr,11 );
		findPairsUsingSort(arr,11);

	}
	
	//method 1 - using extra space
	static void findPairsUsingHash(int[] arr, int sum)
	{   
		Set set = new HashSet();
		
		for(int value :arr)
		{
		int temp = sum - value;
		
		if (!set.contains(temp))
		{
			set.add(value);
			System.out.println(set);
		}
		
		else
		{
			System.out.println("1st value is: " + value + " 2nd value is: " + temp);
		}
		}
	}
	
	//method 2 - in place
	static void findPairsUsingSort(int[] arr,int sum)
	{
		Arrays.sort(arr);
		
		int first =0;
		int last = arr.length-1;
		
		while(first<last)
		{
			int s = arr[first] + arr[last];
			
			if(s==sum)
			{
				System.out.println("1st value is: " + arr[first] + " 2nd value is: " + arr[last]);
				first++;
				last--;
				
				if(arr[last]==arr[last+1])
				{   
					first--;
				}
				
				if(arr[first]==arr[first-1])
				{   
					last++;
				}
			}
			
			else if(s<sum)
			{
				first++;
			}
			else if(s>sum)
			{
				last--;
			}
		}
		
	}

}
