/*Follow up for "Remove Duplicates"
 (Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length. 
 Do not allocate extra space for another array, you must do this in place with constant memory. 
 For example, Given input array A = [1,1,2], Your function should return length = 2, and A is now [1,2].)
 
What if duplicates are allowed at most twice?
For example,
Given sorted array A = [1,1,1,2,2,3],
Your function should return length = 5, and A is now [1,1,2,2,3]
*/
package Sangi;

public class removeDupFromArray2 {

	public static void main(String[] args) 
	{
		int[] arr ={1,1,1,2,2,2,2,3};
		int len = arr.length;
		
		int newLen1 = removeDup1(arr,len);
		
		System.out.println("Length of new array is:" + newLen1);
		System.out.println("Elements of new array are:");
		
		for(int i =0;i<newLen1;i++)
		{
			System.out.println(arr[i]);
		}
		
		/*int newLen = removeDup2(arr,len);
		
		System.out.println("Length of new array is:" +newLen);
		System.out.println("Elements of new array are:");
		
		for(int i =0;i<newLen;i++)
		{
			System.out.println(arr[i]);
		}
		*/

	}
	
	static int removeDup1(int[] arr,int n) //method for elements occuring only once
	{
		if(n==0)
			return 0;
		
		int i=1; 
		
	      for(int j=1; j<n; j++) 
	      {
	        if(arr[j]!=arr[j-1])
	        {
	          arr[i]=arr[j]; 
	          i++; 
	        }
	      }
		
		return i;
		
	}

	
	static int removeDup2(int[] arr,int n) //method for occuring elements atmost twice
	{
		if(n==0)
			return 0;
		
		int occur =1;
		
		int index=0;
		
		for(int i =1;i<n;i++)
		{
			if(arr[index]==arr[i])
			{  
				if(occur==2)
				 continue;
				occur++;
			}
			
			else
			{
				occur=1;
			}
			
			arr[++index] =arr[i];
		}
		
		return index+1;
		
	}

}
