package Sangi;


public class createMinimalBST {

	public static void main(String[] args)
	{
		int[] ascArr = {1,2,3,4,5,6,7,8,9,10,11};
		//int end = ascArr.length-1;
		//int start =0;
		TreeNode k = new TreeNode(ascArr[0]);
		 k =createTree(ascArr);
		 System.out.println(k.value);
		
	}	
	

	static TreeNode createTree(int[] arr)
	{
		return createTree(arr,0,arr.length-1);
	}
	
	static TreeNode createTree(int[] arr,int start,int end)
	{   
		
		int mid = (start+end)/2;
		TreeNode n = new TreeNode(arr[mid]);
		n.left = createTree(arr,start,mid-1);
		n.left = createTree(arr,mid+1,end);
		return n;
	}
}




class TreeNode
{
	public int value;
	public TreeNode left;
	public TreeNode right;
	
	public TreeNode(int value)
	{
		this.value = value;
	}
}

