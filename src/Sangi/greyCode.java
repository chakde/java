/*The gray code is a binary numeral system where two successive values differ in only one bit.

Given a non-negative integer n representing the total number of bits in the code, print the sequence of gray code. A gray code sequence must begin with 0.

For example, given n = 2, return [0,1,3,2]. Its gray code sequence is:
00 - 0
01 - 1
11 - 3
10 - 2
*/

package Sangi;

import java.util.ArrayList;

public class greyCode {

	public static void main(String[] args) 
	{
		//System.out.println(greyCodeGeneration(3));
		System.out.println(greyCode2(4));

	}
	
	/*static ArrayList<Integer> greyCodeGeneration(int n)
	{
		int range = (int)Math.pow(2, n);
		ArrayList<Integer> code = new ArrayList<Integer>();
		
		for(int i =0;i<range;i++)
		{
			code.add(i);
		}
		
		return code;
	}*/
	
	static ArrayList<Integer> greyCode2(int n)
	{
		ArrayList<Integer> results = new ArrayList<Integer>();
		results.add(0);  
		  
		for (int i=0; i < n; ++i)
		{  
		   
		  int size = results.size();
		     
		 for (int j=size-1; j>=0; j--)
		  { 
		       
		       results.add(results.get(j) + size);  
		  }  
		}  
	return results;  
  }  
}


