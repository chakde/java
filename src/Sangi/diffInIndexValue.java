/*Given an array of integers, find out whether there are two distinct indices i and j
 *  in the array such that the difference
 *  between nums[i] and nums[j] is at most t and the difference between i and j is at most k.
 */

package Sangi;

public class diffInIndexValue {

	static int[] nums = {1,2,3,4,2,5,2};
	
	public static void main(String[] args)
	{
		
		System.out.println(Diff(3,1));

	}
	
	static boolean Diff(int t,int k)
	{
        if (k < 0 || t < 0) 
        	return false;
        
        for (int i=0;i<nums.length;i++)
        {
        	 for (int j=i+1;j<nums.length;j++)
        	 {
        		 if(nums[i] -nums[j] <= t && i-j<=k)
        		 {   
        			 System.out.println("i:" + i + " j:"+j);
        			 return true;
        		 }
        	 }
      
	}
        return false;

	}
}
