/*Find the contiguous subarray within an array (containing at least one number) which has the largest product.
For example, given the array [2,3,-2,4],
the contiguous subarray [2,3] has the largest product = 6.
*/

package Sangi;

public class continuosMaxProduct
{
    
	static int[] A ={-4,0,-5,12};
   
	public static void main(String[] args)
	{
		System.out.println("Max continuos product is: " + maxProduct());

	}
	
	 static int maxProduct()
	 {
		    int maxProd = Integer.MIN_VALUE;
	        int prod = 1;
	        
	        for(int i=0; i <A.length; i++)
	        {
	            prod = prod * A[i];
	            maxProd = Math.max(maxProd, prod);
	            
	            if(A[i] == 0)
	            	prod =1;
	           
	        }
	        
	        prod = 1;
	        for(int i=A.length-1; i >=0; i--)
	        {
	            prod = prod * A[i];
	            maxProd = Math.max(maxProd, prod);
	            
	            if(A[i] == 0)
	            	prod =1;
	            
	        }
	        
	        return maxProd;
	    }

}
