package Sangi;

public class CircLL
{
 public static void main(String args[])
 { 
  LLCir list = new LLCir(); 
  //LLCir list1 = new LLCir(); 
  //list1=null;
    // Node_c nodea = new Node_c(2);
     
     list.insert(6);
     list.insert(5);
     list.insert(3);
     list.insert(4);
     list.insert(5);
     //list.delete(3);
     
     System.out.println(list);
    // System.out.println(list1);
    // int x=3;
     Node_c start_pt = list.StartPoint(list);
     System.out.println(start_pt);
     
 }
 
}

class Node_c
{
 int data;
 Node_c next;
 
 public Node_c(int data)
 {
  this.data = data;
  next = null;
 }
 

 
 public String toString(){
  return "Node: "+ data;
 }
}

class LLCir
{
	Node_c head, tail;
 int size = 0 ;
 
 public LLCir()
 { 
 }
 
 public void insert(int data)
 {
  Node_c node = new Node_c(data);
  if(size == 0)
  {
   head = node;
   tail = head;
  }
  else
  {
   tail.next = node;
   tail = node;
  }
  size ++;
 }
 
public String toString()
 {
		String retVal = "";
		if (size == 0)
			return  "( size: "+size+")";
		Node_c node = head;
		while (node != null)
		{
		  retVal = retVal + node.toString() + " ";
		   node = node.next;
		}
		return retVal + "( size: "+size+", head:"+head.data+", tail:"+tail.data+") ";
}
 
 public Node_c StartPoint(LLCir list)
 {
	 Node_c p1 = head;
	 Node_c p2 = head;
	 
	 while(p2.next!=null)
	 {
	 p1= p1.next;
	 p2=p2.next.next;
	 if (p1==p2)
	   break;
	 }
	 
	 if (p2.next==null)
		 return null;
	 
	 p1=head;
	 while(p1!=p2)
	 { p1=p1.next;
	   p2=p2.next;
	   
	 }
	 return p2;
	 }
 
}


