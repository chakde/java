package Sangi;

public class ReverseLinkedList{
	
	public static void main(String[] main)
	{
		LinkLi lst = new LinkLi();
		
		Nodd head = new Nodd(1);
		Nodd one = new Nodd(7);
		Nodd two = new Nodd(2);
		Nodd tww = new Nodd(9);
		
		lst.insertNodes(head);
		lst.insertNodes(one);
		lst.insertNodes(two);
		lst.insertNodes(tww);
		
		Nodd reverseHead=lst.revList(head);
		lst.print(reverseHead);
		
		
	}
	
}

class Nodd{
	
	int data;
	Nodd next;
	
	public Nodd(int data)
	{
		this.data =data;
		next =null;
	}
	
}

class LinkLi
{ 
	Nodd head,tail;
	//Nodd curr =head;
	
	public void insertNodes(Nodd newNode)
	{   
		if (newNode == null)
	       return;
	   else
	   {
		   newNode.next=null;
		   
		   if(head==null)
		   {
			   head = newNode;
			   tail = head;
		   }
		   
		   else
		   {
			   tail.next = newNode;
			   tail = newNode;
		   }
	   }
	
		
	}
	
	void print(Nodd head)
	{//System.out.println("sangi");
		Nodd curr =head;
		while (curr!=null)
		{   
			System.out.println(curr.data);
			curr = curr.next;
			
		}
	}
	
	Nodd revList(Nodd head)
	{ 
		Nodd prev=null;
	    Nodd curr =head;
	    Nodd next;
		
		while (curr!=null)
		{   
		    next = curr.next;
		    
			curr.next =prev;
		    prev =curr;
			curr =next;
		
			
		}
		
		return prev;
	}
}