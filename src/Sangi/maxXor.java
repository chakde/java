package Sangi;

public class maxXor {
	
	public static void main(String[] args)
	{
		int x =max(10,15);
		System.out.println(x);
	}
	
	static int max(int L,int R)
	{   
		int prev_max =0;
		
		for(int a=L;a<=R;a++)
		{
			for(int b =a;b<R;b++)
			{
				int res = a^b;
				
				if(prev_max < res)
					prev_max = res;
				
			}
		}
		
		return prev_max;
	}

}
