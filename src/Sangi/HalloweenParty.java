/*Problem Statement

Alex is attending a Halloween party with his girlfriend, Silvia. At the party, Silvia spots 
the corner of an infinite chocolate bar
 (two dimensional, infinitely long in width and length).

If the chocolate can be served as only 1 x 1 sized pieces and 
Alex can cut the chocolate bar exactly K times, what is the maximum number of chocolate pieces
 Alex can cut and give Silvia?
 * 
 */
package Sangi;
import java.util.Scanner;

public class HalloweenParty {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter the number of cuts:");
		int cuts = in.nextInt();
		int h,v=0;
		
		if(cuts%2 == 0)
			{
			h = cuts/2;
		    v = h;
			}
		else
		   {
			h =cuts/2;
		    v =cuts/2+1;
		   }
		
		int num_of_pieces = h*v;
		System.out.println(num_of_pieces);
		

	}

}
