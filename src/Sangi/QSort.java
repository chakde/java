package Sangi;

public class QSort{
	public static void main(String argv[])

   {
      int A[] = new int[8];
      
         A[0] = 6;
         A[1] =8;
         A[2] = 7;
         A[3] = 64;
         A[4] = 62;
         A[5] = 46;
         A[6] = 64;
         A[7] = 16;

      Quicksort(A, 0, A.length-1);

      for (int i=0 ; i < A.length ; i++)
      System.out.print(A[i] + " ");
      
   }
	
	public static int partition(int A[], int f, int l)
	   { 
		
		  int i = f, j = l;
	      int pivot = A[f];
	      
	      while (i <= j)
	      {
	            while (A[i] < pivot)
	                  i++;
	            while (A[j] > pivot)
	                  j--;
	            if (i <= j)
	            {
	         swap (A, i, j);
	         i++;
	         j--;
	         }
	      }
	      return i;
	   }


public static void swap(int A[],int x,int y){
	
	int temp = A[x];
	A[x] =A[y];
	A[y]=temp;
	
}
public static void Quicksort(int A[], int f, int l)
{
   if (f >= l) return;
   int pivot_index = partition(A, f, l);
   if (f< pivot_index-1)
   Quicksort(A, f, pivot_index-1);
   if (pivot_index <l)
   Quicksort(A, pivot_index, l);
}

}
