/*Given a string containing only digits, restore it by returning all possible valid IP address combinations.
 * 
 */

package Sangi;

import java.util.ArrayList;

public class reStoreIPAddrress {
    
	
	
	public static void main(String[] args)
	{
		
		String ipString ="2512181435";
		System.out.println("List of IP add is:" + restoreIpAddress(ipString));
	

	}
	
	 static ArrayList<String> restoreIpAddress(String s)
	{
		ArrayList<String> res = new ArrayList<String>();
		
		if (s.length()<4||s.length()>12) 
			return res;  
		
		checkSubString(s,"",res,0);
		
		return res;
	}
	 
	 static void checkSubString(String s,String temp,ArrayList<String> res,int count)
	 {   
		
		 for(int i =1; i<4 && i <s.length();i++)
		 {
			 String substr =s.substring(0,i);
			 
			 if (isValid(substr))
			 {   
				 checkSubString(s.substring(i), temp + substr + '.', res, count+1); 
				
	         }  
		 }
		 
		 
		 if (count == 3 && isValid(s))
		 {  
	            res.add(temp + s); 
	            return;  
	     } 
	 }
	 
	 
	 static boolean isValid(String s)
	 {  
	        if (s.charAt(0)=='0')
	        	return s.equals("0"); 
	        
	        int num = Integer.parseInt(s);  
	        
	        return num<=255 && num>0;  
	 }  

}
