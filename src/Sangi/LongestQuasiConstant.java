package Sangi;

import java.util.Stack;

public class LongestQuasiConstant {
	
	int max =1;
	Stack<Integer> stackOfNums=new Stack<Integer>();

	public static void main(String[] args)
	{	
		int[] arr = {10,9,9,9,8};
		LongestQuasiConstant quasi = new LongestQuasiConstant();
		int longestSeq = quasi.longestQuas(arr);
		System.out.println(longestSeq);
	}
		
	int longestQuas(int[] arr)
		 {
		  for(int i=0;i<arr.length;i++)
		   { 
		     stackOfNums = new Stack<Integer>();
		     
		     int min = arr[i];
		     stackOfNums.push(arr[i]);
		
		  for (int j=i+1;j<arr.length;j++)
		   {
		    if (arr[j] - min <=1 && arr[j]>=min)
			{
			   min = Math.min(arr[j],min);
			   stackOfNums.push(arr[j]);
			}
		   }
		  
		  for (int j=i+1;j<arr.length;j++)
		   {
		    if (min - arr[j] <=1 && arr[j]<min)
			{
			   min = Math.min(arr[j],min);
			   stackOfNums.push(arr[j]);
			}
		   }
		   System.out.println("Stack is:" + stackOfNums);
		    max = Math.max(max, stackOfNums.size());
		}
		
		return max;
		     
	 }
	
}
