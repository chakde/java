package Sangi;

import java.util.*;


public class Utopian {

    public static void main(String[] args) 
    {   
       Scanner sc = new Scanner(System.in);
       int n = sc.nextInt();
       height(n);
       
    }
    
    public static void height(int n)
        { 
          int ht =1;
          int prev =1;
          
         for(int i=1;i<=n;i++)
             {
             if (i%2==0)
                 ht =prev+1;
             else
                 ht =2*prev;
             
             }
         prev=ht;
         System.out.println(ht);
         
    }
    
}