/*Given an array with n objects colored red, white or blue, sort them so that objects of the
 *  same color are adjacent, with the colors in the order red, white and blue.
Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.*/

package Sangi;

public class sortColors {

	public static void main(String[] args) {
		
		int[] arr ={0,0,1,1,0,2,1};
		
		int count0=0;
		int count1=0;
		
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]==0)
				count0++;
			if(arr[i]==1)
				count1++;
			
		}
		
		for(int i =0;i<arr.length;i++)
		{
			if(count0>0)
			{
				arr[i]=0;
				count0--;
				continue;
			}
			if(count1>0)
			{
				arr[i]=1;
				count1--;
				continue;
			}
			
			arr[i]=2;
		}
		

		for(int i=0;i<arr.length;i++)
		{
			System.out.println(arr[i]);
		}
		
	}
}
