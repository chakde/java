/*Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

For example,
Given 1->4->3->2->5->2 and x = 3,
return 1->2->2->4->3->5.
*/

/*Approach -
first get the last element and the length of the list
Scan the whole list until the length of list-
if current node value < x, then go to the next node.
else
if current node value >=x, then move this node to the end of the list.
*/

package Sangi;

public class LLpartition {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

	}
	
	class ListNode 
	{   
		 int val;
		 ListNode next;
		 
		 ListNode(int x)
		 {   
			 val =x;
		     next=null;
			 
		 }
		
	
	 ListNode partitionList(ListNode head,int x)
	{
		ListNode p= new ListNode(0);
        p.next = head;
        head = p;
        
		ListNode last =head;
		
        if (head.next==null)
        	return head.next;
		
		int len = 0;
		
		while(last.next!=null)
		{ 
		  last = last.next;
		  len++;	
		  
		}
		
		while(len > 0)
		{
			if(p.next.val <x)
			{
				p=p.next;
				len--;
			}
			else
			{
				last.next = new ListNode(p.next.val);
				last =last.next;
				p.next =p.next.next;
				len--;
			}
		}
		
		return head.next;
	}

}
}