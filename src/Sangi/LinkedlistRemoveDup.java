package Sangi;


public class LinkedlistRemoveDup
{
 public static void main(String args[])
 { 
  MyLinkedList1 list = new MyLinkedList1(); 
     list.insert(1);
     list.insert(2);
     list.insert(5);
     list.insert(4);
     list.insert(3);
     list.insert(2);
     list.insert(9);
     list.insert(8);
     list.insert(10);
     System.out.println(list);
     list.deleteDups2(list);
     System.out.println(list);
     //Testing the recommit
     
 }
 
}

class Node_test
{
 int data;
 Node_test next;
 
 public Node_test(int data)
 {
  this.data = data;
  next = null;
 }
 

 
 public String toString(){
  return "Node: "+ data;
 }
}

class MyLinkedList1
{
 Node_test head, tail;
 int size = 0 ;
 
 public MyLinkedList1()
 { 
 }
 
 public void insert(int data)
 {
  Node_test node = new Node_test(data);
  if(size == 0)
  {
   head = node;
   tail = head;
  }
  else
  {
   tail.next = node;
   tail = node;
  }
  size ++;
 }
 
 public  void deleteDups2(MyLinkedList1 list)
 {
	 if (head == null) return;
	 Node_test previous = head;
	 Node_test current = previous.next;
	 //System.out.println(current);
	 while (current != null)
	 {
		 Node_test runner = head;
	 while (runner != current) 
	 { // Check for earlier dups
	  if (runner.data == current.data) 
	  {
	  Node_test tmp = current.next; // remove current
	  previous.next = tmp;
	  current = tmp; // update current to next node
	  break; // all other dups have already been removed
	  }
	  runner = runner.next;
	  }
	  if (runner == current) 
	  { // current not updated - update now
	  previous = current;
	  current = current.next;
	  }
	  }
 }
	  
 
 
 

 
 public String toString()
 {
		String retVal = "";
		if (size == 0)
			return  "( size: "+size+")";
		Node_test node = head;
		while (node != null)
		{
		  retVal = retVal + node.toString() + " ";
		   node = node.next;
		}
		return retVal + "( size: "+size+", head:"+head.data+", tail:"+tail.data+") ";
}
 

}


