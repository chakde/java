package Sangi;

import java.util.Scanner;
import java.util.Stack;

public class LargestRectangle
{
	 public static void main(String[] args)
	 { 		 
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter number of buildings:");
		 String numInput = sc.nextLine();
		 
	     int n = Integer.parseInt(numInput);
	     System.out.println("Enter height of buildings:");
		 String[] input = sc.nextLine().split(" ");
		 
		 Stack<Integer> stack =new Stack<Integer>();
		 Stack<Integer> stack1 =new Stack<Integer>();
		 for(int ctr=0;ctr<n;ctr++)
		 {		
			 stack.push(Integer.parseInt(input[ctr]));			 
		 }
		 
		 sc.close();
		 
		/*Stack<Integer> stack =new Stack<Integer>();
		Stack<Integer> stack1 =new Stack<Integer>();
		
		stack.push(7);
		stack.push(1);
		stack.push(8);
		stack.push(3);
		stack.push(5);*/ 	 
		
		int maxArea = 0;
		for(int index=0;index<stack.size();index++)
		{
			int area = 	calcMAx(stack,stack1);
			if(maxArea<area)
				maxArea = area;			
			
			int stackSize = stack1.size();
			for(int ptr = 0;ptr<stackSize -1 ;ptr++)
			{				
			   int newEle = stack1.pop();
			   stack.push(newEle);
			}
			stack1.clear();
		}
		System.out.println("Maxarea is:" + maxArea );
	}
 
	static int calcMAx(Stack<Integer> stack,Stack<Integer> stack1)  
	 {		   
		   int min = 1000000;
		   int prevMax = 0;
		   int max = 0;
		   
		 while (!stack.isEmpty())
		 {
			 int height = stack.pop();
			 int size=stack1.size()+1;
			
			 if(height < min)
				 min=height;
			 
			 max = maxArea(min, size);
			 stack1.push(height);
			 if(prevMax < max)
			 {
			 	prevMax =max;
			 }
		 }		 
		 return prevMax;
	 }
	
	static int maxArea(int ht,int size)
	{ 
		int area =0;
	    area = (ht*size);
		return area;
	}

  
}
