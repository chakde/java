/*given a list of words,write a program to find the longest word made of other words*/
 package Sangi;
 

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class modLongestWordInaList {

	public static void main(String[] args)
	{
		String[] strs = {"man", "dog", "dogjan","cat","sangeetagillgillgill","dogmanman"};
		
		Arrays.sort(strs, new StringLengthComparator());
		String test =printLongestWord(strs);
		
		if (test != " ")
		System.out.println("Longest word made of other words is: " +test);
		else
	    System.out.println("No such word");
	}

	
	static String printLongestWord(String[] strs)
	{
		HashMap<String,Boolean> map = new HashMap<String,Boolean>();//for easy lookup
		
		for(String str:strs)
		{
			map.put(str, true);
		}

		for(int i =strs.length-1;i>=0;i--)
		{
			if(canBuildWord(strs[i],true,map))
			  return strs[i];
	    }
		return " ";
	}
	
	static boolean canBuildWord(String str,boolean isOriginalWord,HashMap<String,Boolean> map)
	{   
		if(map.containsKey(str) && !isOriginalWord)
		{
			return map.get(str);
		}
		
		for(int i=0; i<str.length(); i++)
		{
			String left = str.substring(0,i);
			String right = str.substring(i);
			
			if (map.containsKey(left) && canBuildWord(right,false,map))
			{
				return true;
			}
			
		}
		
		map.put(str, false);
		return false;
		
	}
	}


class StringLengthComparator implements Comparator<String> 
{
	  public int compare(String o1, String o2) 
	  {
	    if (o1.length() < o2.length())
          return -1;
	   if (o1.length() > o2.length())
	      return 1;
	   else
	    return 0;
	    
      }
}